import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';

import { PageLayoutComponent } from './layouts/page-layout/page-layout.component';

import { ConfirmDialogComponent } from './commons/components/confirm-dialog/confirm-dialog.component';
import { KeyValueListComponent } from './commons/components/key-value-list/key-value-list.component';

import { LoginComponent } from './sections/login/pages/login/login.component';

import { WelcomeComponent } from './sections/welcome/pages/welcome/welcome.component';

import { TaskComponent } from './sections/task/pages/task/task.component';
import { TaskToolbarComponent } from './sections/task/components/task-toolbar/task-toolbar.component';
import { TaskEditComponent } from './sections/task/components/task-edit/task-edit.component';
import { TaskDetailsComponent } from './sections/task/components/task-details/task-details.component';
import { TaskCreateComponent } from './sections/task/components/task-create/task-create.component';
import { TaskListComponent } from './sections/task/components/task-list/task-list.component';
import { TaskListItemComponent } from './sections/task/components/task-list-item/task-list-item.component';
import { TaskSummaryComponent } from './sections/task/components/task-summary/task-summary.component';

import { BlogArticlesComponent } from './sections/blog/pages/blog-articles/blog-articles.component';
import { BlogArticleListComponent } from './sections/blog/components/blog-article-list/blog-article-list.component';
import { BlogArticleListItemComponent } from './sections/blog/components/blog-article-list-item/blog-article-list-item.component';

import { MaterialModule } from './modules/material.module';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RoutingModule } from './modules/routing/routing.module';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { BlogArticleEditorComponent } from './sections/blog/pages/blog-article-editor/blog-article-editor.component';

@NgModule({
  declarations: [
    AppComponent,

    PageLayoutComponent,

    ConfirmDialogComponent,
    KeyValueListComponent,

    LoginComponent,

    WelcomeComponent,

    TaskComponent,
    TaskCreateComponent,
    TaskDetailsComponent,
    TaskEditComponent,
    TaskListComponent,
    TaskListItemComponent,
    TaskSummaryComponent,
    TaskToolbarComponent,

    BlogArticlesComponent,
    BlogArticleListComponent,
    BlogArticleListItemComponent,
    BlogArticleEditorComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    RoutingModule,
    MaterialModule,
    ServiceWorkerModule.register('ngsw-worker.js', {
      enabled: environment.production,
      registrationStrategy: 'registerWhenStable:30000',
    }),
  ],
  providers: [],
  bootstrap: [
    AppComponent,
  ],
})
export class AppModule { }
