import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { routes } from './routes';

const routerModuleConfig: Routes =
  routes.map(({ path, component, canActivate } ) => ({ path, component, canActivate }));

@NgModule({
  imports: [ RouterModule.forRoot(routerModuleConfig) ],
  exports: [ RouterModule ],
})
export class RoutingModule { }
