import { BlogArticlesComponent } from "src/app/sections/blog/pages/blog-articles/blog-articles.component";
import { LoginComponent } from "src/app/sections/login/pages/login/login.component";
import { TaskComponent } from "src/app/sections/task/pages/task/task.component";
import { WelcomeComponent } from "src/app/sections/welcome/pages/welcome/welcome.component";
import { AuthGuard } from "./auth.guard";

export const routes = [
  {
    path: 'login',
    component: LoginComponent,
  },
  {
    icon: 'home',
    label: 'Home',
    path: '',
    component: WelcomeComponent,
    canActivate: [ AuthGuard ],
  },
  {
    path: 'tasks/:id',
    component: TaskComponent,
    canActivate: [ AuthGuard ],
  },
  {
    icon: 'checklist',
    label: 'Tasks',
    path: 'tasks',
    component: TaskComponent,
    canActivate: [ AuthGuard ],
  },
  // {
  //   path: 'blog/articles/:id',
  //   component: BlogArticleEditorComponent,
  //   canActivate: [ AuthGuard ],
  // },
  {
    icon: 'rss_feed',
    label: 'Blog',
    path: 'blog/articles',
    component: BlogArticlesComponent,
    canActivate: [ AuthGuard ],
  },
];
