import { Injectable } from '@angular/core';
import { CanActivate, Router, UrlTree } from '@angular/router';
import { SessionService } from 'src/app/commons/services/session/session.service';

@Injectable({
  providedIn: 'root',
})
export class AuthGuard implements CanActivate {
  private sessionService: SessionService;
  private router: Router;

  public constructor(sessionService: SessionService, router: Router) {
    this.sessionService = sessionService;
    this.router = router;
  }

  public canActivate(): boolean | UrlTree {
    return this.sessionService.isValid()
      ? true
      : this.router.parseUrl('/login');
  }
}
