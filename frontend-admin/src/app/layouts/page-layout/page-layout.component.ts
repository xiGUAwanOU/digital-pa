import { BreakpointObserver } from '@angular/cdk/layout';
import { Component, OnDestroy, ViewChild } from '@angular/core';
import { MatDrawer, MatDrawerMode } from '@angular/material/sidenav';
import { Subscription } from 'rxjs';
import { routes } from 'src/app/modules/routing/routes';

@Component({
  selector: 'app-page-layout',
  templateUrl: './page-layout.component.html',
  styleUrls: [ './page-layout.component.scss' ],
})
export class PageLayoutComponent implements OnDestroy {
  public narrowDisplay: boolean;
  public navDisplayMode: MatDrawerMode;
  public routes = routes
    .filter((route) => route.icon && route.label)
    .map(({ icon, label, path }) => ({ icon, label, link: `/${path}` }));

  public get showMenuButton(): boolean {
    return this.narrowDisplay;
  }

  public get menuOpenedInitialState(): boolean {
    return !this.showMenuButton;
  }

  @ViewChild('drawer') private drawer!: MatDrawer;

  private breakPointObserverSubscription?: Subscription;

  public constructor(breakPointObserver: BreakpointObserver) {
    this.narrowDisplay = true;
    this.navDisplayMode = 'over';

    this.breakPointObserverSubscription = breakPointObserver
      .observe('(min-width: 1100px)')
      .subscribe((breakPointMatchResult) => {
        if (!breakPointMatchResult.matches) {
          this.navDisplayMode = 'over';
          this.narrowDisplay = true;
          this.closeDrawer();
        } else {
          this.narrowDisplay = false;
          this.navDisplayMode = 'side';
          this.openDrawer();
        }
      });
  }

  public ngOnDestroy() {
    this.breakPointObserverSubscription?.unsubscribe();
  }

  public openDrawer() {
    setTimeout(() => {
      this.drawer.open();
    });
  }

  public closeDrawer() {
    if (this.showMenuButton) {
      setTimeout(() => {
        this.drawer.close();
      });
    }
  }
}
