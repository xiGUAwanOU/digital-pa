import { Router } from "@angular/router";
import { Component } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import jwtDecode from "jwt-decode";
import { HttpService } from 'src/app/commons/services/http/http.service';
import { SessionService } from 'src/app/commons/services/session/session.service';
import { LoginParameter, LoginResult } from 'src/../../backend/src/services/auth/types';
import { AuthSection, Login } from 'src/../../backend/src/services/auth/constants';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: [ './login.component.scss' ],
})
export class LoginComponent {
  public password: FormControl;

  public get errorMessage(): string {
    if (this.password.hasError('incorrect')) {
      return 'Login failed! Please check your password';
    }

    if (this.password.hasError('required')) {
      return 'Password cannot be empty';
    }

    return '';
  }

  private httpService: HttpService;
  private sessionService: SessionService;
  private router: Router;

  public constructor(
    httpService: HttpService,
    sessionService: SessionService,
    router: Router,
  ) {
    this.password = new FormControl('', [ Validators.required ]);
    this.httpService = httpService;
    this.sessionService = sessionService;
    this.router = router;
  }

  public async onSubmit() {
    try {
      const result = await this.httpService.call<LoginParameter, LoginResult>(AuthSection, Login, {
        password: this.password.value,
      });

      const decodedJwt = jwtDecode<{ exp: number }>(result.jwt);

      this.sessionService.create({
        expirationTimeEpochMillis: decodedJwt.exp * 1000,
      });

      this.router.navigate([ '/' ]);
    } catch(error) {
      this.password.setErrors({ incorrect: 'Login failed! Please check your password.' });
      this.password.setValue('');
    }
  }
}
