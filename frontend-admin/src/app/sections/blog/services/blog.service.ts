import { Injectable } from "@angular/core";
import { HttpService } from "src/app/commons/services/http/http.service";

import {
  BlogSection,
} from 'src/../../backend/src/services/blog/constants';

import {
  ArticleResult,
  CreateArticleParameter,
  CreateArticleResult,
  DeleteArticleParameter,
  GetArticleParameter,
  ListArticlesParameter,
  UpdateArticleParameter,
} from 'src/../../backend/src/services/blog/article/types';
import {
  CreateArticle,
  DeleteArticle,
  GetArticle,
  ListArticles,
  UpdateArticle,
} from 'src/../../backend/src/services/blog/article/constants';

import {
  TagResult,
  CreateTagParameter,
  CreateTagResult,
  DeleteTagParameter,
  GetTagParameter,
  ListTagsParameter,
  UpdateTagParameter,
} from 'src/../../backend/src/services/blog/tag/types';
import {
  CreateTag,
  DeleteTag,
  GetTag,
  ListTags,
  UpdateTag,
} from 'src/../../backend/src/services/blog/tag/constants';

@Injectable()
export class BlogService {
  private httpService: HttpService;
  
  public constructor(httpService: HttpService) {
    this.httpService = httpService;
  }

  public async listArticles(param: ListArticlesParameter): Promise<ArticleResult[]> {
    return await this.httpService.call<ListArticlesParameter, ArticleResult[]>(BlogSection, ListArticles, {
      tagIds: param.tagIds,
    });
  }

  public async getArticle(param: GetArticleParameter): Promise<ArticleResult> {
    return await this.httpService.call<GetArticleParameter, ArticleResult>(BlogSection, GetArticle, {
      id: param.id,
    });
  }

  public async createArticle(param: CreateArticleParameter) {
    await this.httpService.call<CreateArticleParameter, CreateArticleResult>(BlogSection, CreateArticle, {
      title: param.title,
      content: param.content,
      tagIds: param.tagIds,
      time: param.time,
      location: param.location,
      thumbnailImageUrl: param.thumbnailImageUrl,
      published: param.published,
    });
  }

  public async updateArticle(param: UpdateArticleParameter) {
    await this.httpService.call<UpdateArticleParameter, void>(BlogSection, UpdateArticle, {
      id: param.id,
      title: param.title,
      content: param.content,
      tagIds: param.tagIds,
      time: param.time,
      location: param.location,
      thumbnailImageUrl: param.thumbnailImageUrl,
      published: param.published,
    });
  }

  public async deleteArticle(param: DeleteArticleParameter) {
    await this.httpService.call<DeleteArticleParameter, void>(BlogSection, DeleteArticle, {
      id: param.id,
    });
  }

  public async listTags(): Promise<TagResult[]> {
    return await this.httpService.call<ListTagsParameter, TagResult[]>(BlogSection, ListTags, {});
  }

  public async getTag(param: GetTagParameter): Promise<TagResult> {
    return await this.httpService.call<GetTagParameter, TagResult>(BlogSection, GetTag, {
      id: param.id,
    });
  }

  public async createTag(param: CreateTagParameter) {
    await this.httpService.call<CreateTagParameter, CreateTagResult>(BlogSection, CreateTag, {
      name: param.name,
    });
  }

  public async updateTag(param: UpdateTagParameter) {
    await this.httpService.call<UpdateTagParameter, void>(BlogSection, UpdateTag, {
      id: param.id,
      name: param.name,
    });
  }

  public async deleteTag(param: DeleteTagParameter) {
    await this.httpService.call<DeleteTagParameter, void>(BlogSection, DeleteTag, {
      id: param.id,
    });
  }
}
