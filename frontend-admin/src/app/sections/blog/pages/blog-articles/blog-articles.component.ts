import { Component } from '@angular/core';
import { ArticleResult } from 'src/../../backend/src/services/blog/article/types';

@Component({
  selector: 'app-blog-articles',
  templateUrl: './blog-articles.component.html',
  styleUrls: [ './blog-articles.component.scss' ],
})
export class BlogArticlesComponent {
  public articles: ArticleResult[] = [
    {
      id: '000000000000000000000000',
      createdAt: '2021-12-29T17:04:00.000Z',
      updatedAt: '2021-12-29T17:04:00.000Z',
      title: [
        {
          language: 'en',
          content: 'Test Article',
        },
      ],
      content: [
        {
          language: 'en',
          content: '<p>Hello world!</p>',
          excerpt: 'Hello world!',
        },
      ],
      tagIds: [ '000000000000000000000000', '000000000000000000000001' ],
      time: {
        datetime: '2021-11-07T00:00:00.000Z',
        timezone: 'Europe/Berlin',
      },
      location: {
        name: 'At Home',
        latitude: 0,
        longitude: 0,
      },
      thumbnailImageUrl: 'https://i.stack.imgur.com/2OrtT.jpg',
      published: false,
    },
    {
      id: '000000000000000000000001',
      createdAt: '2021-12-29T17:04:00.000Z',
      updatedAt: '2021-12-29T17:04:00.000Z',
      title: [
        {
          language: 'zh',
          content: '测试文章',
        },
      ],
      content: null,
      tagIds: [],
      time: {
        datetime: '2021-11-07T00:00:00.000Z',
        timezone: 'Europe/Berlin',
      },
      location: null,
      thumbnailImageUrl: null,
      published: true,
    },
  ];

  public constructor() { }
}
