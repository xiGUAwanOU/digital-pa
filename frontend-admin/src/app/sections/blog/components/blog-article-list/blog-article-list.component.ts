import { Component, Input, EventEmitter, Output } from '@angular/core';
import { ArticleResult } from 'src/../../backend/src/services/blog/article/types';

@Component({
  selector: 'app-blog-article-list',
  templateUrl: './blog-article-list.component.html',
  styleUrls: [ './blog-article-list.component.scss' ],
})
export class BlogArticleListComponent {
  @Input() public articles: ArticleResult[] = [];
  @Output() public update = new EventEmitter<void>();

  public constructor() {}

  public getArticleId(index: number, article: ArticleResult) {
    return article.id;
  }
}
