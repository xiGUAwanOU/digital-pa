import * as moment from 'moment-timezone';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ArticleResult } from 'src/../../backend/src/services/blog/article/types';
import { BlogLanguageContent } from 'src/../../backend/src/services/blog/types';
import { Router } from '@angular/router';

@Component({
  selector: 'app-blog-article-list-item',
  templateUrl: './blog-article-list-item.component.html',
  styleUrls: [ './blog-article-list-item.component.scss' ],
})
export class BlogArticleListItemComponent implements OnInit {
  @Input() public article!: ArticleResult;
  @Output() public update = new EventEmitter<void>();

  public formattedDate!: string;
  public tags!: string;

  private router: Router;

  public constructor(router: Router) {
    this.router = router;
  }

  public async ngOnInit() {
    this.formattedDate = moment(this.article.time.datetime)
      .tz(this.article.time.timezone)
      .format('DD MMM YYYY');

    this.tags = this.article.tagIds
      .map((tagId, index) => [ 'Motorcycling', 'Hiking' ][index % 2])
      .join(' / ');
  }

  public goToBlogArticle() {
    this.router.navigate([ '/blog/articles', this.article.id ]);
  }

  public getBlogLanguageContent<T>(blogLanguageContents: BlogLanguageContent<T>[]): T {
    const blogLanguageContent = blogLanguageContents.find((c) => c.language === 'zh');
    const fallbackBlogLanguageContent = blogLanguageContents[0];

    return blogLanguageContent || fallbackBlogLanguageContent;
  }
}
