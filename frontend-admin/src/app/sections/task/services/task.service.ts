import { Injectable } from '@angular/core';
import { HttpService } from 'src/app/commons/services/http/http.service';
import {
  CreateTaskParameter,
  CreateTaskResult,
  DeleteFinishedSubTasksParameter,
  DeleteTaskParameter,
  GetTaskParameter,
  ListTasksParameter,
  ReopenFinishedSubTasksParameter,
  TaskResult,
  ToggleTaskFinishedParameter,
  UpdateTaskParameter,
} from 'src/../../backend/src/services/task/types';
import {
  CreateTask,
  DeleteFinishedSubTasks,
  DeleteTask,
  GetTask,
  ListTasks,
  ReopenFinishedSubTasks,
  TaskSection,
  ToggleTaskFinished,
  UpdateTask,
} from 'src/../../backend/src/services/task/constants';

@Injectable()
export class TaskService {
  private httpService: HttpService;

  public constructor(httpService: HttpService) {
    this.httpService = httpService;
  }

  public async listTasks(param: ListTasksParameter): Promise<TaskResult[]> {
    return await this.httpService.call<ListTasksParameter, TaskResult[]>(TaskSection, ListTasks, {
      parentId: param.parentId,
    });
  }

  public async getTask(param: GetTaskParameter): Promise<TaskResult> {
    return await this.httpService.call<GetTaskParameter, TaskResult>(TaskSection, GetTask, {
      id: param.id,
    });
  }

  public async createTask(param: CreateTaskParameter) {
    await this.httpService.call<CreateTaskParameter, CreateTaskResult>(TaskSection, CreateTask, {
      parentId: param.parentId,
      content: param.content,
      finished: param.finished,
    });
  }

  public async updateTask(param: UpdateTaskParameter) {
    await this.httpService.call<UpdateTaskParameter, void>(TaskSection, UpdateTask, {
      id: param.id,
      parentId: param.parentId,
      content: param.content,
      finished: param.finished,
    });
  }

  public async toggleTaskFinished(param: ToggleTaskFinishedParameter) {
    await this.httpService.call<ToggleTaskFinishedParameter, void>(TaskSection, ToggleTaskFinished, {
      id: param.id,
    });
  }

  public async reopenFinishedSubTasks(param: ReopenFinishedSubTasksParameter) {
    await this.httpService.call<ReopenFinishedSubTasksParameter, void>(TaskSection, ReopenFinishedSubTasks, {
      id: param.id,
    });
  }

  public async deleteTask(param: DeleteTaskParameter) {
    await this.httpService.call<DeleteTaskParameter, void>(TaskSection, DeleteTask, {
      id: param.id,
    });
  }

  public async deleteFinishedSubTasks(param: DeleteFinishedSubTasksParameter) {
    await this.httpService.call<DeleteFinishedSubTasksParameter, void>(TaskSection, DeleteFinishedSubTasks, {
      id: param.id,
    });
  }
}
