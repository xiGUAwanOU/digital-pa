import { Component, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { TaskResult } from 'src/../../backend/src/services/task/types';
import { TaskService } from '../../services/task.service';

@Component({
  selector: 'app-task',
  templateUrl: './task.component.html',
  styleUrls: [ './task.component.scss' ],
  providers: [ TaskService ],
})
export class TaskComponent implements OnDestroy {
  public taskId: string | null;
  public task: TaskResult | null;
  public subTasks: TaskResult[];

  private route: ActivatedRoute;
  private routeParamsSubscription: Subscription;
  private service: TaskService;

  public constructor(route: ActivatedRoute, service: TaskService) {
    this.taskId = null;
    this.task = null;
    this.subTasks = [];

    this.route = route;
    this.service = service;

    this.routeParamsSubscription = this.route.params.subscribe((routeParams) => {
      this.taskId = routeParams.id || null;
      this.reloadData();
    });
  }
  
  public ngOnDestroy() {
    this.routeParamsSubscription.unsubscribe();
  }

  public async reloadData() {
    this.task = this.taskId ? (await this.service.getTask({ id: this.taskId })) : null;
    this.subTasks = await this.service.listTasks({ parentId: this.taskId });
  }
}
