import { Component, EventEmitter, Input, Output } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { ConfirmDialogComponent } from 'src/app/commons/components/confirm-dialog/confirm-dialog.component';
import { TaskDetailsComponent } from '../task-details/task-details.component';
import { TaskEditComponent } from '../task-edit/task-edit.component';
import { TaskResult } from 'src/../../backend/src/services/task/types';
import { TaskService } from '../../services/task.service';
import { lastValueFrom } from 'rxjs';

@Component({
  selector: 'app-task-toolbar',
  templateUrl: './task-toolbar.component.html',
  styleUrls: [ './task-toolbar.component.scss' ],
  providers: [ TaskService ],
})
export class TaskToolbarComponent {
  @Input() public task!: TaskResult;
  @Output() public update = new EventEmitter<void>();

  private dialog: MatDialog;
  private router: Router;
  private service: TaskService;

  public constructor(dialog: MatDialog, router: Router, service: TaskService) {
    this.dialog = dialog;
    this.router = router;
    this.service = service;
  }

  public goUpToParentTask() {
    if (!this.task.parentId) {
      this.router.navigate([ '/tasks' ]);
      return;
    }

    this.router.navigate([ '/tasks', this.task.parentId ]);
  }

  public showTaskDetails() {
    this.dialog.open(TaskDetailsComponent, {
      autoFocus: false,
      disableClose: true,
      minWidth: '300px',
      width: '38vw',
      data: {
        task: this.task,
      },
    });
  }

  public async editTask() {
    const updatedTask = await lastValueFrom(this.dialog.open(TaskEditComponent, {
      autoFocus: false,
      disableClose: true,
      minWidth: '300px',
      width: '38vw',
      data: {
        task: this.task,
      },
    }).afterClosed());

    if (!updatedTask) {
      return;
    }

    await this.service.updateTask({
      id: this.task.id,
      parentId: updatedTask.parentId,
      content: updatedTask.content,
      finished: updatedTask.finished,
    });

    this.update.emit();
  }

  public async deleteTask() {
    const confirmed = await lastValueFrom(this.dialog.open(ConfirmDialogComponent, {
      autoFocus: false,
      disableClose: true,
      data: {
        title: 'Delete Task',
        text: 'Delete a task will also delete all its sub-tasks. Do you want to continue?',
      },
    }).afterClosed());

    if (!confirmed) {
      return;
    }

    await this.service.deleteTask({ id: this.task.id });

    if (!this.task.parentId) {
      this.router.navigate([ '/tasks' ]);
      return;
    }

    this.router.navigate([ '/tasks', this.task.parentId ]);
  }

  public async reopenFinishedSubTasks() {
    const confirmed = await lastValueFrom(this.dialog.open(ConfirmDialogComponent, {
      autoFocus: false,
      disableClose: true,
      data: {
        title: 'Re-open Finished Tasks',
        text: 'All finished tasks will be re-opened, which will not affect the sub-tasks. Do you want to continue?',
      },
    }).afterClosed());

    if (!confirmed) {
      return;
    }

    await this.service.reopenFinishedSubTasks({ id: this.task.id });

    this.update.emit();
  }

  public async deleteFinishedSubTasks() {
    const confirmed = await lastValueFrom(this.dialog.open(ConfirmDialogComponent, {
      autoFocus: false,
      disableClose: true,
      data: {
        title: 'Delete Finished Tasks',
        text: 'All finished tasks with their sub-tasks will be deleted. Do you want to continue?',
      },
    }).afterClosed());

    if (!confirmed) {
      return;
    }

    await this.service.deleteFinishedSubTasks({ id: this.task.id });

    this.update.emit();
  }
}
