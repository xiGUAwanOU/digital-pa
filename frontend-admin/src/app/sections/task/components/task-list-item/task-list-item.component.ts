import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { TaskResult } from 'src/../../backend/src/services/task/types';
import { TaskService } from '../../services/task.service';

@Component({
  selector: 'app-task-list-item',
  templateUrl: './task-list-item.component.html',
  styleUrls: [ './task-list-item.component.scss' ],
  providers: [ TaskService ],
})
export class TaskListItemComponent implements OnInit {
  @Input() public task!: TaskResult;
  @Output() public update = new EventEmitter<void>();

  public numOfFinishedSubTasks!: number;
  public numOfSubTasks!: number;

  private service: TaskService;
  private router: Router;

  public constructor(service: TaskService, router: Router) {
    this.service = service;
    this.router = router;
  }

  public async ngOnInit() {
    const subTasks = await this.service.listTasks({ parentId: this.task.id });

    this.numOfSubTasks = subTasks.length;
    this.numOfFinishedSubTasks = subTasks.filter((task) => task.finished).length;
  }

  public goToSubTask() {
    this.router.navigate([ '/tasks', this.task.id ]);
  }

  public async toggleTaskFinished() {
    await this.service.toggleTaskFinished({ id: this.task.id });
    
    this.update.emit();
  }
}
