import { Component, EventEmitter, Input, Output } from '@angular/core';
import { TaskResult } from 'src/../../backend/src/services/task/types';

@Component({
  selector: 'app-task-list',
  templateUrl: './task-list.component.html',
  styleUrls: [ './task-list.component.scss' ],
})
export class TaskListComponent {
  @Input() public tasks: TaskResult[] = [];
  @Output() public update = new EventEmitter<void>();

  public constructor() { }

  public getTaskId(index: number, task: TaskResult) {
    return task.id;
  }
}
