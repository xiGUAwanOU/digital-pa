import { Component, EventEmitter, Input, Output } from '@angular/core';
import { FormControl } from '@angular/forms';
import { TaskService } from '../../services/task.service';

@Component({
  selector: 'app-task-create',
  templateUrl: './task-create.component.html',
  styleUrls: [ './task-create.component.scss' ],
  providers: [ TaskService ],
})
export class TaskCreateComponent {
  @Input() public parentId: string | null = null;
  @Output() public update = new EventEmitter<void>();
  
  public content: FormControl;

  private service: TaskService;

  public constructor(service: TaskService) {
    this.content = new FormControl('');

    this.service = service;
  }

  public async createTask() {
    if (!this.content.value || !this.content.value.trim()) {
      return;
    }

    await this.service.createTask({
      parentId: this.parentId,
      content: this.content.value,
      finished: false,
    });

    this.content.setValue('');

    this.update.emit();
  }
}
