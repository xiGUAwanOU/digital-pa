import { Component, Inject } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { TaskResult } from 'src/../../backend/src/services/task/types';

@Component({
  selector: 'app-task-edit',
  templateUrl: './task-edit.component.html',
  styleUrls: [ './task-edit.component.scss' ],
})
export class TaskEditComponent {
  public task: TaskResult;

  public parentId: FormControl;
  public content: FormControl;
  public finished: boolean;

  public get updatedTask(): TaskResult {
    return {
      ...this.task,
      id: this.task.id,
      parentId: this.parentId.value || null,
      content: this.content.value,
      finished: this.finished,
    };
  }

  public constructor(@Inject(MAT_DIALOG_DATA) { task }: { task: TaskResult }) {
    this.task = task;
    this.parentId = new FormControl(this.task.parentId);
    this.content = new FormControl(this.task.content);
    this.finished = this.task.finished;
  }
}
