import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { KeyValueListDivider, KeyValuePairs } from 'src/app/commons/components/key-value-list/key-value-list.types';
import { TaskResult } from 'src/../../backend/src/services/task/types';

@Component({
  selector: 'app-task-details',
  templateUrl: './task-details.component.html',
  styleUrls: [ './task-details.component.scss' ],
})
export class TaskDetailsComponent {
  public task: TaskResult;

  public get taskKeyValuePairs(): KeyValuePairs {
    return [
      { key: 'ID', value: this.task.id },
      { key: 'Parent ID', value: this.task.parentId || 'null' },
      { key: 'Content', value: this.task.content },
      { key: 'Finished', value: this.task.finished ? 'Yes' : 'No' },

      KeyValueListDivider,

      { key: 'Created At', value: this.task.createdAt },
      { key: 'Updated At', value: this.task.updatedAt },
    ]
  }

  public constructor(@Inject(MAT_DIALOG_DATA) { task }: { task: TaskResult }) {
    this.task = task;
  }
}
