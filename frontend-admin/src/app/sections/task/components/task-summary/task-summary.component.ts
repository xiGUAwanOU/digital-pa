import { Component, Input } from '@angular/core';
import { TaskResult } from 'src/../../backend/src/services/task/types';

@Component({
  selector: 'app-task-summary',
  templateUrl: './task-summary.component.html',
  styleUrls: [ './task-summary.component.scss' ],
})
export class TaskSummaryComponent {
  @Input() public task!: TaskResult;
  @Input() public subTasks!: TaskResult[];

  public get numOfFinishedSubTasks(): number {
    return this.subTasks.filter((task) => task.finished).length;
  }

  public get numOfSubTasks(): number {
    return this.subTasks.length;
  }

  public get finishedPercent(): string {
    if (this.numOfSubTasks === 0) {
      return '0%';
    }
    return (this.numOfFinishedSubTasks / this.numOfSubTasks * 100).toFixed(0) + '%';
  }

  public constructor() { }
}
