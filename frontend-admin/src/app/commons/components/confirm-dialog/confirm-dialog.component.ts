import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ConfirmDialogData } from './confirm-dialog.types';

@Component({
  selector: 'app-confirm-dialog',
  templateUrl: './confirm-dialog.component.html',
})
export class ConfirmDialogComponent {
  public data: ConfirmDialogData;

  public constructor(@Inject(MAT_DIALOG_DATA) data: ConfirmDialogData) {
    this.data = data;
  }
}
