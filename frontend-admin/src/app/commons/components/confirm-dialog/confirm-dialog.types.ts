export interface ConfirmDialogData {
  title: string;
  text: string;
  negativeLabel?: string;
  positiveLabel?: string;
}
