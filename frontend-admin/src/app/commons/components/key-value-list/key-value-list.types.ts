export interface KeyValuePair {
  key: string;
  value: string;
}

export type KeyValueListDivider = typeof KeyValueListDivider;

export type KeyValuePairs = (KeyValuePair | KeyValueListDivider)[];

export const KeyValueListDivider = 'KEY_VALUE_LIST_DIVIDER';