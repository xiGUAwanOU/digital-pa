import { Component, Input } from '@angular/core';
import { Clipboard } from '@angular/cdk/clipboard';
import { KeyValueListDivider, KeyValuePair, KeyValuePairs } from './key-value-list.types';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-key-value-list',
  templateUrl: './key-value-list.component.html',
  styleUrls: [ './key-value-list.component.scss' ],
})
export class KeyValueListComponent {
  @Input() public keyValuePairs!: KeyValuePairs;

  public identifyKeyValuePair = (index: number, keyValuePair: KeyValuePair | KeyValueListDivider) => {
    return this.isKeyValuePair(keyValuePair) ? keyValuePair.key : index;
  }

  private clipboard: Clipboard;
  private snapkBar: MatSnackBar;

  public constructor(clipboard: Clipboard, snackBar: MatSnackBar) {
    this.clipboard = clipboard;
    this.snapkBar = snackBar;
  }

  public isKeyValuePair(value: KeyValuePair | KeyValueListDivider): value is KeyValuePair {
    const keyValuePair = value as KeyValuePair;
    return keyValuePair.key !== undefined && keyValuePair.value !== undefined;
  }

  public copyValue(value: string) {
    this.clipboard.copy(value);
    this.snapkBar.open(`Value copied to clipboard`, undefined, {
      duration: 2000,
    });
  }
}
