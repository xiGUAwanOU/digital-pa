import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { HttpService } from './http.service';
import { HttpError } from './http.types';

interface TestParameter {
  foo: string;
  bar: string;
}

interface TestResult {
  message: string;
}

describe('HttpService', () => {
  let service: HttpService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ HttpClientTestingModule ],
      providers: [ HttpService ],
    });

    service = TestBed.inject(HttpService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  afterEach(() => {
    httpMock.verify();
  });

  it('should send request and return response', async () => {
    const resultPromise = service.call<TestParameter, TestResult>('test', 'test', {
      foo: 'baz',
      bar: 'qux',
    });

    const endpointMock = httpMock.expectOne('/api/test/test');
    endpointMock.flush({
      message: 'Hello world!',
    });

    const result = await resultPromise;

    expect(endpointMock.request.method).toBe('POST');
    expect(endpointMock.request.body).toEqual({
      foo: 'baz',
      bar: 'qux',
    });
    expect(result).toEqual({
      message: 'Hello world!',
    });
  });

  it('should throw wrapped error', async () => {
    const resultPromise = service.call<TestParameter, TestResult>('test', 'test', {
      foo: 'baz',
      bar: 'qux',
    });

    const endpointMock = httpMock.expectOne('/api/test/test');
    endpointMock.flush({
      error: 'UnknownError',
      message: 'There is an error for test',
    }, {
      status: 400,
      statusText: 'Bad Request',
    });

    expect(endpointMock.request.method).toBe('POST');
    expect(endpointMock.request.body).toEqual({
      foo: 'baz',
      bar: 'qux',
    });
    await expectAsync(resultPromise)
      .toBeRejectedWith(new HttpError('UnknownError', 'There is an error for test'));
  });
});
