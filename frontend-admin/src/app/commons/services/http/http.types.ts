export class HttpError extends Error {
  public constructor(name: ErrorType, message: string) {
    super(message);
    this.name = name;
  }
}

export type ErrorType =
  'UnknownError' |
  'ServiceExecutionError' |
  'ServiceNotFoundError' |
  'ServicePermissionDeniedError' |
  'ServiceParameterInvalidError';
