import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { lastValueFrom } from 'rxjs';
import { HttpError } from './http.types';

@Injectable({
  providedIn: 'root',
})
export class HttpService {
  private readonly http: HttpClient;

  public constructor(http: HttpClient) {
    this.http = http;
  }

  public async call<P, R>(section: string, action: string, parameter: P): Promise<R> {
    try {
      return lastValueFrom(this.http.post<R>(
        `/api/${section}/${action}`,
        parameter,
        { responseType: 'json' },
      ));
    } catch (e) {
      const error = e as HttpErrorResponse;

      if (error.status === 0) {
        throw new HttpError('UnknownError', 'HTTP request failed');
      }

      if (!error.error.error || !error.error.message) {
        throw new HttpError('UnknownError', error.message);
      }

      throw new HttpError(error.error.error, error.error.message);
    }
  }
}
