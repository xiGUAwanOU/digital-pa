export interface Session {
  expirationTimeEpochMillis: number;
}
