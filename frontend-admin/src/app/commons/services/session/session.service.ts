import { Injectable } from '@angular/core';
import { Session } from './session.types';

const LOCAL_STORAGE_FIELD = 'session';

@Injectable({
  providedIn: 'root',
})
export class SessionService {
  public constructor() { }

  public create(session: Session) {
    localStorage.setItem(LOCAL_STORAGE_FIELD, JSON.stringify(session));
  }

  public destroy() {
    localStorage.removeItem(LOCAL_STORAGE_FIELD);
  }

  public isValid(): boolean {
    const serializedSession = localStorage.getItem(LOCAL_STORAGE_FIELD);

    if (!serializedSession) {
      return false;
    }

    let session: Session;
    try {
      session = JSON.parse(serializedSession);
    } catch {
      return false;
    }

    if (Date.now() > session.expirationTimeEpochMillis) {
      return false;
    }

    return true;
  }
}
