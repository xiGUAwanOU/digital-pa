# Digital PA

## Introduction

Digital PA should be a **flexible** and **automated** personal assistant software for **private usage**:
* flexible means that it is fully extendable and configurable, while
* automated means that there is a lot of possibilities to create automated tasks, so that the user (myself) doesn't need to do repetitive manual works, and finally
* private usage means that unless other specified, all the contents should be only accessible by a single user (myself).

## Overall Structure

There are 3 sub-projects in this project:
* `backend`: the part which implements all the business logic.
* `frontend-admin`: the user interface for the backend.
* `frontend-public`: a blog-like user interface for selectively published contents.

## Code Structure: backend

The backend project consists of 2 different parts. `commons` and `services`:
* `commons` are the reusable code, which are used while implementing business logics.
  * `common/config` is the global configuration of the backend part.
  * `common/context` is the singleton registry of the backend part.
  * `common/framework-integration` implements the necessary adapters to make the other parts working with the HTTP framework, currently it is express.
  * `common/service-registry` implements a dynamic service registry from where services can be identified and called.
* `servicces` are the implementation of the concrete business logic, they are groupped by sections (or "domains").

The "services" are identified by their sections and functionalities, for example, `task/list-tasks`, or `auth/login`.

Externally, the URL of the corresponding endpoints are named after these two parts, e.g. `https://hostname/api/task/list-tasks`, and the verb are mostly `POST`. Internally, the services can call each other by providing the name and the parameter of the services. The system should be aware of different services dynamically at the runtime, so that later on we can use this as part of the automation.

## Code Structure: frontend-admin

## Code Structure: frontend-public

## Deployment

To deploy the application, simply run following commands on development machine:

```console
$ devops/build-and-deploy.sh <SSH_identity_file> <IP_address> <server_app_path>
```
