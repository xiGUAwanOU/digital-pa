const path = require('path');
const TsconfigPathsPlugin = require('tsconfig-paths-webpack-plugin');

module.exports = {
  entry: './src/main.ts',
  target: 'node',
  module: {
    rules: [
      {
        test: /\.ts$/,
        use: 'ts-loader',
      },
    ],
  },
  resolve: {
    extensions: [ '.ts', '.js' ],
    plugins: [ new TsconfigPathsPlugin({ configFile: path.resolve(__dirname, 'tsconfig.json') }) ],
  },
  externals: [
    'bson-ext',
    'kerberos',
    'snappy',
    'aws4',
    'mongodb-client-encryption',
  ],
  output: {
    filename: 'main.js',
    path: path.resolve(__dirname, 'dist'),
  },
};
