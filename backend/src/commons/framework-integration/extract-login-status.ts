import * as jwt from 'jsonwebtoken';
import { RequestHandler } from 'express';
import { GlobalContext } from 'src/commons/context/global-context';

export function ExtractLoginStatus(ctx: GlobalContext): RequestHandler {
  return (req, res, next) => {
    try {
      const jwtString = req.cookies['Authorization'];
      jwt.verify(jwtString, ctx.config.jwt.secret);
      req.userLoggedIn = true;
    } catch (err) {
      req.userLoggedIn = false;
    }

    next();
  };
}
