declare namespace Express {
  export interface Request {
    userLoggedIn: boolean
  }
}
