import * as express from 'express';
import * as CookieParser from 'cookie-parser';
import { Request, Response, RequestHandler } from 'express';

import { GlobalContext } from 'src/commons/context/global-context';

import { ExtractLoginStatus } from './extract-login-status';

export type ParameterExtractor = (req: Request) => any | Promise<any>;
export type ResultHandler = (result: any, res: Response) => void | Promise<void>;
export type ErrorHandler = (error: Error, res: Response) => void | Promise<void>;

interface RegisterServiceCallingEndpointOptions {
  httpMethod?: 'post' | 'get' | 'put' | 'patch' | 'delete';
  urlSuffix?: string;
  preHandlers?: RequestHandler[];
  parameterExtractor?: ParameterExtractor;
  resultHandler?: ResultHandler;
  errorHandler?: ErrorHandler;
}

const defaultParameterExtractor: ParameterExtractor = (req) => req.body;
const defaultResultHandler: ResultHandler = (result, res) => {
  if (result) {
    res.status(200);
    res.json(result);
    res.end();
  } else {
    res.status(200);
    res.end();
  }
};
const defaultErrorHandler: ErrorHandler = (error, res) => {
  res.status(400);
  res.json({
    error: error.name,
    message: error.message,
  });
  res.end();
};

export function RegisterEndpoint(
  ctx: GlobalContext,
  section: string,
  name: string,
  options?: RegisterServiceCallingEndpointOptions,
) {
  const httpMethod = options?.httpMethod ?? 'post';
  const url = `/api/${section}/${name}` + (options?.urlSuffix || '');
  const preHandlers = options?.preHandlers ?? [ express.json() ];
  const parameterExtractor = options?.parameterExtractor ?? defaultParameterExtractor;
  const resultHandler = options?.resultHandler ?? defaultResultHandler;
  const errorHandler = options?.errorHandler ?? defaultErrorHandler;

  ctx.express[httpMethod](
    url,
    CookieParser(),
    ExtractLoginStatus(ctx),
    ...preHandlers,
    async (req, res) => {
      const parameter = await parameterExtractor(req);
      const authParameters = { userLoggedIn: req.userLoggedIn };

      try {
        const result = await ctx.serviceRegistry.callService(section, name, parameter, authParameters);
        await resultHandler(result, res);
      } catch(error) {
        await errorHandler(error as Error, res);
      }
    },
  );
}
