export interface GlobalConfig {
  primaryPassword: string;
  jwt: {
    secret: string;
    expiresInSeconds: number;
  };
  database: {
    host: string;
    port: number;
    name: string;
    user: string;
    password: string;
  };
}
