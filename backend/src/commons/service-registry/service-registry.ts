import {
  ServiceDefinition,
  ServiceAuthorizationParameters,
  ServiceExecutionError,
  ServiceNotFoundError,
  ServiceParameterInvalidError,
  ServicePermissionDeniedError,
  ServiceError,
} from './types';

export interface ServiceQueryContext {
  userLoggedIn: boolean;
}

export class ServiceRegistry {
  private services: {
    [section: string]: {
      [name: string]: ServiceDefinition;
    };
  };

  public constructor() {
    this.services = {};
  }

  public registerService(service: ServiceDefinition) {
    if (!this.services[service.section]) {
      this.services[service.section] = {};
    }

    this.services[service.section][service.name] = service;
  }

  public async callService<P, R>(
    section: string,
    name: string,
    parameter: P,
    authParameters: ServiceAuthorizationParameters,
  ): Promise<R> {
    const call = async <CP = any, CR = any>(section: string, name: string, parameter: CP): Promise<CR> => {
      if (!this.services[section] || !this.services[section][name]) {
        throw new ServiceNotFoundError(section, name);
      }

      const service = this.services[section][name];

      if (!service.validation(parameter)) {
        throw new ServiceParameterInvalidError(section, name, parameter);
      }

      if (!service.authorization(authParameters, parameter)) {
        throw new ServicePermissionDeniedError(section, name, parameter);
      }

      try {
        return await service.call(parameter, { call });
      } catch(e) {
        if (e instanceof ServiceError) {
          throw new ServiceExecutionError(section, name, e.message);
        }

        throw e;
      }
    };

    return await call<P, R>(section, name, parameter);
  }
}
