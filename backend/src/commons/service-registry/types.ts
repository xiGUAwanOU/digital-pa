export interface ServiceDefinition<P = any, R = any> {
  section: string;
  name: string;
  validation: ServiceValidationFunction<P>;
  authorization: ServiceAuthorizationFunction<P>;
  call: (parameter: P, call: ServiceCaller) => R | Promise<R>;
}

export type ServiceValidationFunction<P> = (parameter: P) => boolean;
export type ServiceAuthorizationFunction<P> = (permission: ServiceAuthorizationParameters, parameter: P) => boolean;

export interface ServiceAuthorizationParameters {
  userLoggedIn: boolean;
}

export interface ServiceCaller {
  call<P = any, R = any>(section: string, name: string, parameter: P): R | Promise<R>;
}

export class ServiceError extends Error {
  public constructor(message: string) {
    super(message);
    this.name = 'ServiceError';
  }
}

export class ServiceExecutionError extends Error {
  public constructor(section: string, name: string, message: string) {
    super(`An error occurs while calling Function ${section}/${name}: ${message}`);
    this.name = 'ServiceExecutionError';
  }
}

export class ServiceNotFoundError extends Error {
  public constructor(section: string, name: string) {
    super(`Service ${section}/${name} cannot be found`);
    this.name = 'ServiceNotFoundError';
  }
}

export class ServicePermissionDeniedError extends Error {
  public constructor(section: string, name: string, parameter: any) {
    super(`Insufficient permission to call function ${section}/${name} with parameter: ${JSON.stringify(parameter)}`);
    this.name = 'ServicePermissionDeniedError';
  }
}

export class ServiceParameterInvalidError extends Error {
  public constructor(section: string, name: string, parameter: any) {
    super(`The parameter passed to function ${section}/${name} is invalid: ${JSON.stringify(parameter)}`);
    this.name = 'ServiceParameterInvalidError';
  }
}
