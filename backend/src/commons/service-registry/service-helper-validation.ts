import * as Ajv from 'ajv';

function NotRequired() {
  return () => true;
}

function WithJsonSchema<P>(schema: any) {
  const ajv = new Ajv();
  const validateFunction = ajv.compile(schema);

  return (parameter: P) =>
    !!validateFunction(parameter);
}

function WithJsonSchemaAnd<P>(schema: any, predicate: (parameter: P) => boolean) {
  const ajv = new Ajv();
  const validateFunction = ajv.compile(schema);

  return (parameter: P) =>
    !!validateFunction(parameter) && predicate(parameter);
}

export const Validation = {
  NotRequired,
  WithJsonSchema,
  WithJsonSchemaAnd,
};
