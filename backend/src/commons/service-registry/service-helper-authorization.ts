import { ServiceAuthorizationParameters } from './types';

function NotRequired() {
  return () => true;
}

function WithLoggedInStatus() {
  return (permission: ServiceAuthorizationParameters) =>
    permission.userLoggedIn;
}

function WithLoggedInStatusAnd<P>(predicate: (parameter: P) => boolean) {
  return (permission: ServiceAuthorizationParameters, parameter: P) =>
    permission.userLoggedIn && predicate(parameter);
}

function WithLoggedInStatusOr<P>(predicate: (parameter: P) => boolean) {
  return (permission: ServiceAuthorizationParameters, parameter: P) =>
    permission.userLoggedIn || predicate(parameter);
}

export const Authorization = {
  NotRequired,
  WithLoggedInStatus,
  WithLoggedInStatusAnd,
  WithLoggedInStatusOr,
};
