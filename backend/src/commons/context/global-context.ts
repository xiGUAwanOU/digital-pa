import { Express } from 'express';
import { Db, MongoClient } from 'mongodb';
import { GlobalConfig } from 'src/commons/config/global-config';
import { ServiceRegistry } from 'src/commons/service-registry/service-registry';

export interface GlobalContext {
  config: GlobalConfig;
  express: Express;
  databaseConnection: MongoClient;
  database: Db;
  serviceRegistry: ServiceRegistry;
}
