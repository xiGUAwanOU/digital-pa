import { GlobalContext } from 'src/commons/context/global-context';
import { ObjectId } from 'mongodb';
import { ServiceDefinition, ServiceError } from 'src/commons/service-registry/types';
import { Authorization } from 'src/commons/service-registry/service-helper-authorization';
import { Validation } from 'src/commons/service-registry/service-helper-validation';
import { GetTask, TaskCollection, TaskSection } from './constants';
import {
  GetTaskParameter,
  GetTaskParameterSchema,
  TaskResult,
} from './types';

export function BuildGetTaskService(ctx: GlobalContext): ServiceDefinition<GetTaskParameter, TaskResult> {
  return {
    section: TaskSection,
    name: GetTask,
    validation: Validation.WithJsonSchema(GetTaskParameterSchema),
    authorization: Authorization.WithLoggedInStatus(),
    call: async (param) => {
      const result = await ctx.database.collection(TaskCollection).findOne({ _id: new ObjectId(param.id) });

      if (!result) {
        throw new ServiceError('Entity not found');
      }

      return {
        id: result._id.toHexString(),

        createdAt: result.createdAt,
        updatedAt: result.updatedAt,

        parentId: result.parentId,
        content: result.content,
        finished: result.finished,
      };
    },
  };
}
