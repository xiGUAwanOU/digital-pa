import { GlobalContext } from 'src/commons/context/global-context';
import { ServiceDefinition } from 'src/commons/service-registry/types';
import { Authorization } from 'src/commons/service-registry/service-helper-authorization';
import { Validation } from 'src/commons/service-registry/service-helper-validation';
import {
  CreateTaskParameter,
  CreateTaskParameterSchema,
  CreateTaskResult,
} from './types';
import {
  CreateTask,
  TaskCollection,
  TaskSection,
} from './constants';

export function BuildCreateTaskService(ctx: GlobalContext): ServiceDefinition<CreateTaskParameter, CreateTaskResult> {
  return {
    section: TaskSection,
    name: CreateTask,
    validation: Validation.WithJsonSchema(CreateTaskParameterSchema),
    authorization: Authorization.WithLoggedInStatus(),
    call: async (param) => {
      const currentDatetime = new Date();

      const result = await ctx.database.collection(TaskCollection).insertOne({
        createdAt: currentDatetime,
        updatedAt: currentDatetime,

        parentId: param.parentId,
        content: param.content,
        finished: param.finished,
      });

      return { id: result.insertedId.toHexString() };
    },
  };
}
