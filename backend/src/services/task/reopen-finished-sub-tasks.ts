import { GlobalContext } from 'src/commons/context/global-context';
import { ServiceDefinition } from 'src/commons/service-registry/types';
import { Authorization } from 'src/commons/service-registry/service-helper-authorization';
import { Validation } from 'src/commons/service-registry/service-helper-validation';
import {
  ReopenFinishedSubTasksParameter,
  ReopenFinishedSubTasksParameterSchema,
  ListTasksParameter,
  TaskResult,
  ToggleTaskFinishedParameter,
} from './types';
import {
  ListTasks,
  ReopenFinishedSubTasks,
  TaskSection,
  ToggleTaskFinished,
} from './constants';

export function BuildReopenFinishedSubTasksService(
  _ctx: GlobalContext,
): ServiceDefinition<ReopenFinishedSubTasksParameter, void> {
  return {
    section: TaskSection,
    name: ReopenFinishedSubTasks,
    validation: Validation.WithJsonSchema(ReopenFinishedSubTasksParameterSchema),
    authorization: Authorization.WithLoggedInStatus(),
    call: async (param, caller) => {
      const children = await caller.call<ListTasksParameter, TaskResult[]>(
        TaskSection,
        ListTasks,
        {
          parentId: param.id,
        },
      );

      const toggleTaskFinishedPromises = children
        .filter((child) => child.finished)
        .map((child) => caller.call<ToggleTaskFinishedParameter, void>(
          TaskSection,
          ToggleTaskFinished,
          {
            id: child.id,
          }
        ));

      await Promise.all(toggleTaskFinishedPromises);
    },
  };
}
