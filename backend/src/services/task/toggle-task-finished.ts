import { ObjectId } from 'mongodb';
import { GlobalContext } from 'src/commons/context/global-context';
import { ServiceDefinition, ServiceError } from 'src/commons/service-registry/types';
import { Authorization } from 'src/commons/service-registry/service-helper-authorization';
import { Validation } from 'src/commons/service-registry/service-helper-validation';
import {
  TaskCollection,
  TaskSection,
  ToggleTaskFinished,
} from './constants';
import {
  ToggleTaskFinishedParameter,
  ToggleTaskFinishedParameterSchema,
} from './types';

export function BuildToggleTaskFinishedService(
  ctx: GlobalContext,
): ServiceDefinition<ToggleTaskFinishedParameter, void> {
  return {
    section: TaskSection,
    name: ToggleTaskFinished,
    validation: Validation.WithJsonSchema(ToggleTaskFinishedParameterSchema),
    authorization: Authorization.WithLoggedInStatus(),
    call: async (param) => {
      const currentDatetime = new Date();

      const result = await ctx.database.collection(TaskCollection).findOne({ _id: new ObjectId(param.id) });

      if (!result) {
        throw new ServiceError('Entity not found');
      }

      await ctx.database.collection(TaskCollection).updateOne(
        { _id: new ObjectId(param.id) },
        {
          $set: {
            updatedAt: currentDatetime,
            finished: !result.finished,
          },
        },
      );
    },
  };
}
