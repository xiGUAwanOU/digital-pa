import { ObjectId } from 'mongodb';
import { GlobalContext } from 'src/commons/context/global-context';
import { ServiceDefinition, ServiceError } from 'src/commons/service-registry/types';
import { Authorization } from 'src/commons/service-registry/service-helper-authorization';
import { Validation } from 'src/commons/service-registry/service-helper-validation';
import { SetParentTask, TaskCollection, TaskSection } from './constants';
import {
  SetParentTaskParameter,
  SetParentTaskParameterSchema,
} from './types';

export function BuildSetParentTaskService(ctx: GlobalContext): ServiceDefinition<SetParentTaskParameter, void> {
  return {
    section: TaskSection,
    name: SetParentTask,
    validation: Validation.WithJsonSchema(SetParentTaskParameterSchema),
    authorization: Authorization.WithLoggedInStatus(),
    call: async (param) => {
      const currentDatetime = new Date();

      const result = await ctx.database.collection(TaskCollection).updateOne(
        { _id: new ObjectId(param.id) },
        {
          $set: {
            updatedAt: currentDatetime,
            parentId: param.parentId,
          },
        },
      );

      if (result.matchedCount === 0) {
        throw new ServiceError('Entity not found');
      }
    },
  };
}
