import { GlobalContext } from 'src/commons/context/global-context';
import { RegisterEndpoint } from 'src/commons/framework-integration/register-endpoint';

import { BuildCreateTaskService } from './create-task';
import { BuildGetTaskService } from './get-task';
import { BuildListTasksService } from './list-tasks';
import { BuildUpdateTaskService } from './update-task';
import { BuildSetParentTaskService } from './set-parent-task';
import { BuildToggleTaskFinishedService } from './toggle-task-finished';
import { BuildReopenFinishedSubTasksService } from './reopen-finished-sub-tasks';
import { BuildDeleteTaskService } from './delete-task';
import { BuildDeleteFinishedSubTasksService } from './delete-finished-sub-tasks';
import {
  CreateTask,
  DeleteFinishedSubTasks,
  DeleteTask,
  GetTask,
  ListTasks,
  ReopenFinishedSubTasks,
  SetParentTask,
  TaskSection,
  ToggleTaskFinished,
  UpdateTask,
} from './constants';

export function RegisterTaskServices(ctx: GlobalContext) {
  ctx.serviceRegistry.registerService(BuildCreateTaskService(ctx));
  ctx.serviceRegistry.registerService(BuildGetTaskService(ctx));
  ctx.serviceRegistry.registerService(BuildListTasksService(ctx));
  ctx.serviceRegistry.registerService(BuildUpdateTaskService(ctx));
  ctx.serviceRegistry.registerService(BuildToggleTaskFinishedService(ctx));
  ctx.serviceRegistry.registerService(BuildReopenFinishedSubTasksService(ctx));
  ctx.serviceRegistry.registerService(BuildSetParentTaskService(ctx));
  ctx.serviceRegistry.registerService(BuildDeleteTaskService(ctx));
  ctx.serviceRegistry.registerService(BuildDeleteFinishedSubTasksService(ctx));
}

export function RegisterTaskEndpoints(ctx: GlobalContext) {
  RegisterEndpoint(ctx, TaskSection, CreateTask);
  RegisterEndpoint(ctx, TaskSection, GetTask);
  RegisterEndpoint(ctx, TaskSection, ListTasks);
  RegisterEndpoint(ctx, TaskSection, UpdateTask);
  RegisterEndpoint(ctx, TaskSection, SetParentTask);
  RegisterEndpoint(ctx, TaskSection, ToggleTaskFinished);
  RegisterEndpoint(ctx, TaskSection, ReopenFinishedSubTasks);
  RegisterEndpoint(ctx, TaskSection, DeleteTask);
  RegisterEndpoint(ctx, TaskSection, DeleteFinishedSubTasks);
}
