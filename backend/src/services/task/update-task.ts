import { ObjectId } from 'mongodb';
import { GlobalContext } from 'src/commons/context/global-context';
import { ServiceDefinition, ServiceError } from 'src/commons/service-registry/types';
import { Authorization } from 'src/commons/service-registry/service-helper-authorization';
import { Validation } from 'src/commons/service-registry/service-helper-validation';
import {
  TaskCollection,
  TaskSection,
  UpdateTask,
} from './constants';
import {
  UpdateTaskParameter,
  UpdateTaskParameterSchema,
} from './types';

export function BuildUpdateTaskService(ctx: GlobalContext): ServiceDefinition<UpdateTaskParameter, void> {
  return {
    section: TaskSection,
    name: UpdateTask,
    validation: Validation.WithJsonSchema(UpdateTaskParameterSchema),
    authorization: Authorization.WithLoggedInStatus(),
    call: async (param) => {
      const currentDatetime = new Date();

      const result = await ctx.database.collection(TaskCollection).updateOne(
        { _id: new ObjectId(param.id) },
        {
          $set: {
            updatedAt: currentDatetime,

            parentId: param.parentId,
            content: param.content,
            finished: param.finished,
          },
        },
      );

      if (result.matchedCount === 0) {
        throw new ServiceError('Entity not found');
      }
    },
  };
}
