import { GlobalContext } from 'src/commons/context/global-context';
import { ServiceDefinition } from 'src/commons/service-registry/types';
import { Authorization } from 'src/commons/service-registry/service-helper-authorization';
import { Validation } from 'src/commons/service-registry/service-helper-validation';
import {
  ListTasks,
  TaskCollection,
  TaskSection,
} from './constants';
import {
  ListTasksParameter,
  ListTasksParameterSchema,
  TaskResult,
} from './types';

export function BuildListTasksService(ctx: GlobalContext): ServiceDefinition<ListTasksParameter, TaskResult[]> {
  return {
    section: TaskSection,
    name: ListTasks,
    validation: Validation.WithJsonSchema(ListTasksParameterSchema),
    authorization: Authorization.WithLoggedInStatus(),
    call: async (param) => {
      const results = await ctx.database.collection(TaskCollection).find({
        parentId: param.parentId,
      }).toArray();

      return results.map((result) => ({
        id: result._id.toHexString(),

        createdAt: result.createdAt,
        updatedAt: result.updatedAt,

        parentId: result.parentId,
        content: result.content,
        finished: result.finished,
      }));
    },
  };
}
