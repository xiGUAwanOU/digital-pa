import { GlobalContext } from 'src/commons/context/global-context';
import { ServiceDefinition } from 'src/commons/service-registry/types';
import { Authorization } from 'src/commons/service-registry/service-helper-authorization';
import { Validation } from 'src/commons/service-registry/service-helper-validation';
import {
  DeleteFinishedSubTasksParameter,
  DeleteFinishedSubTasksParameterSchema,
  ListTasksParameter,
  TaskResult,
  DeleteTaskParameter,
} from './types';
import {
  DeleteFinishedSubTasks,
  DeleteTask,
  ListTasks,
  TaskSection,
} from './constants';

export function BuildDeleteFinishedSubTasksService(
  _ctx: GlobalContext,
): ServiceDefinition<DeleteFinishedSubTasksParameter, void> {
  return {
    section: TaskSection,
    name: DeleteFinishedSubTasks,
    validation: Validation.WithJsonSchema(DeleteFinishedSubTasksParameterSchema),
    authorization: Authorization.WithLoggedInStatus(),
    call: async (param, caller) => {
      const children = await caller.call<ListTasksParameter, TaskResult[]>(
        TaskSection,
        ListTasks,
        {
          parentId: param.id,
        },
      );

      const deleteTaskPromises = children
        .filter((child) => child.finished)
        .map((child) => caller.call<DeleteTaskParameter, void>(
          TaskSection,
          DeleteTask,
          {
            id: child.id,
          }
        ));

      await Promise.all(deleteTaskPromises);
    },
  };
}
