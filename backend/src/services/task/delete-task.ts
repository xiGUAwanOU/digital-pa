import { GlobalContext } from 'src/commons/context/global-context';
import { ObjectId } from 'mongodb';
import { ServiceDefinition, ServiceError } from 'src/commons/service-registry/types';
import { Authorization } from 'src/commons/service-registry/service-helper-authorization';
import { Validation } from 'src/commons/service-registry/service-helper-validation';
import {
  DeleteTask,
  TaskCollection,
  TaskSection,
} from './constants';
import {
  DeleteTaskParameter,
  DeleteTaskParameterSchema,
} from './types';

export function BuildDeleteTaskService(ctx: GlobalContext): ServiceDefinition<DeleteTaskParameter, void> {
  async function recursivelyDelete(id: string) {
    const result = await ctx.database.collection(TaskCollection).deleteOne({ _id: new ObjectId(id) });

    if (result.deletedCount === 0) {
      throw new ServiceError('Entity not found');
    }

    const childIds = (await ctx.database.collection(TaskCollection).find({ parentId: id }).toArray())
      .map((child) => child._id.toHexString());

    await Promise.all(childIds.map((childId) => recursivelyDelete(childId)));
  }

  return {
    section: TaskSection,
    name: DeleteTask,
    validation: Validation.WithJsonSchema(DeleteTaskParameterSchema),
    authorization: Authorization.WithLoggedInStatus(),
    call: async (param) => {
      await recursivelyDelete(param.id);
    },
  };
}
