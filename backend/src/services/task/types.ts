interface TaskId {
  id: string;
}

interface TaskData {
  parentId: string | null;
  content: string;
  finished: boolean;
}

interface TaskMetaData {
  createdAt: string;
  updatedAt: string;
}

const idSchema = { type: 'string', pattern: '^[0-9a-f]{24}$' };

const parentIdSchema = { type: [ 'string', 'null' ], pattern: '^[0-9a-f]{24}$' };
const contentSchema = { type: 'string' };
const finishedSchema = { type: 'boolean' };

const TaskIdSchema = {
  properties: {
    id: idSchema,
  },
  required: [ 'id' ],
};

const TaskDataSchema = {
  properties: {
    parentId: parentIdSchema,
    content: contentSchema,
    finished: finishedSchema,
  },
  required: [ 'parentId', 'content', 'finished' ],
};

export type CreateTaskParameter = TaskData;

export const CreateTaskParameterSchema = {
  type: 'object',
  properties: {
    ...TaskDataSchema.properties,
  },
  required: [
    ...TaskDataSchema.required,
  ],
  additionalProperties: false,
};

export type CreateTaskResult = TaskId;

export interface ListTasksParameter {
  parentId: string | null;
}

export const ListTasksParameterSchema = {
  type: 'object',
  properties: {
    parentId: parentIdSchema,
  },
  required: [
    'parentId',
  ],
  additionalProperties: false,
};

export type GetTaskParameter = TaskId;

export const GetTaskParameterSchema = {
  type: 'object',
  properties: {
    ...TaskIdSchema.properties,
  },
  required: [
    ...TaskIdSchema.required,
  ],
  additionalProperties: false,
};

export interface TaskResult extends TaskId, TaskMetaData, TaskData {}

export interface UpdateTaskParameter extends TaskData, TaskId {}

export const UpdateTaskParameterSchema = {
  type: 'object',
  properties: {
    ...TaskIdSchema.properties,
    ...TaskDataSchema.properties,
  },
  required: [
    ...TaskIdSchema.required,
    ...TaskDataSchema.required,
  ],
  additionalProperties: false,
};

export type ToggleTaskFinishedParameter = TaskId;

export const ToggleTaskFinishedParameterSchema = {
  type: 'object',
  properties: {
    ...TaskIdSchema.properties,
  },
  required: [
    ...TaskIdSchema.required,
  ],
  additionalProperties: false,
};

export interface SetParentTaskParameter extends TaskId {
  parentId: string | null;
}

export const SetParentTaskParameterSchema = {
  type: 'object',
  properties: {
    ...TaskIdSchema.properties,
    parentId: parentIdSchema,
  },
  required: [
    ...TaskIdSchema.required,
    'parentId',
  ],
  additionalProperties: false,
};

export type ReopenFinishedSubTasksParameter = TaskId;

export const ReopenFinishedSubTasksParameterSchema = {
  type: 'object',
  properties: {
    ...TaskIdSchema.properties,
  },
  required: [
    ...TaskIdSchema.required,
  ],
  additionalProperties: false,
};

export type DeleteTaskParameter = TaskId;

export const DeleteTaskParameterSchema = {
  type: 'object',
  properties: {
    ...TaskIdSchema.properties,
  },
  required: [
    ...TaskIdSchema.required,
  ],
  additionalProperties: false,
};

export type DeleteFinishedSubTasksParameter = TaskId;

export const DeleteFinishedSubTasksParameterSchema = {
  type: 'object',
  properties: {
    ...TaskIdSchema.properties,
  },
  required: [
    ...TaskIdSchema.required,
  ],
  additionalProperties: false,
};
