import { GlobalContext } from 'src/commons/context/global-context';
import { RegisterArticleServices, RegsiterArticleEndpoints } from './article';
import { RegisterTagServices, RegsiterTagEndpoints } from './tag';

export function RegisterBlogServices(ctx: GlobalContext) {
  RegisterArticleServices(ctx);
  RegisterTagServices(ctx);
}

export function RegsiterBlogEndpoints(ctx: GlobalContext) {
  RegsiterArticleEndpoints(ctx);
  RegsiterTagEndpoints(ctx);
}
