export type Language = 'zh' | 'en' | 'de';

export type MultiLangContent<T> = {
  [key in Language]?: T;
};

export const MultiLangContentSchema = (innerSchema: any) => ({
  type: 'object',
  properties: [
    'zh',
    'en',
    'de',
  ].reduce((obj, lang) => ({ ...obj, [lang]: innerSchema }), {}),
  minProperties: 1,
  additionalProperties: false,
});
