import { GlobalContext } from 'src/commons/context/global-context';
import { ServiceDefinition } from 'src/commons/service-registry/types';
import { Authorization } from 'src/commons/service-registry/service-helper-authorization';
import { Validation } from 'src/commons/service-registry/service-helper-validation';
import { BlogSection } from '../constants';
import {
  TagCollection,
  ListTags,
} from './constants';
import {
  TagResult,
  ListTagsParameter,
  ListTagsParameterSchema,
} from './types';

export function BuildListTagsService(
  ctx: GlobalContext,
): ServiceDefinition<ListTagsParameter, TagResult[]> {
  return {
    section: BlogSection,
    name: ListTags,
    validation: Validation.WithJsonSchema(ListTagsParameterSchema),
    authorization: Authorization.WithLoggedInStatus(),
    call: async () => {
      const results = await ctx.database.collection(TagCollection).find({}).toArray();

      return results.map((result) => ({
        id: result._id.toHexString(),

        createdAt: result.createdAt,
        updatedAt: result.updatedAt,

        name: result.name,
      }));
    },
  };
}
