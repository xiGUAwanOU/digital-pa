import { ServiceDefinition } from 'src/commons/service-registry/types';
import { Authorization } from 'src/commons/service-registry/service-helper-authorization';
import { Validation } from 'src/commons/service-registry/service-helper-validation';
import { GlobalContext } from 'src/commons/context/global-context';
import { BlogSection } from '../constants';
import {
  TagCollection,
  CreateTag,
} from './constants';
import {
  CreateTagParameter,
  CreateTagParameterSchema,
  CreateTagResult,
} from './types';

export function BuildCreateTagService(
  ctx: GlobalContext,
): ServiceDefinition<CreateTagParameter, CreateTagResult> {
  return {
    section: BlogSection,
    name: CreateTag,
    validation: Validation.WithJsonSchema(CreateTagParameterSchema),
    authorization: Authorization.WithLoggedInStatus(),
    call: async (param) => {
      const currentDatetime = new Date();

      const result = await ctx.database.collection(TagCollection).insertOne({
        createdAt: currentDatetime,
        updatedAt: currentDatetime,

        name: param.name,
      });

      return { id: result.insertedId.toHexString() };
    },
  };
}
