import { ObjectId } from 'mongodb';
import { GlobalContext } from 'src/commons/context/global-context';
import { ServiceDefinition, ServiceError } from 'src/commons/service-registry/types';
import { Authorization } from 'src/commons/service-registry/service-helper-authorization';
import { Validation } from 'src/commons/service-registry/service-helper-validation';
import { BlogSection } from '../constants';
import {
  TagCollection,
  UpdateTag,
} from './constants';
import {
  UpdateTagParameter,
  UpdateTagParameterSchema,
} from './types';

export function BuildUpdateTagService(ctx: GlobalContext): ServiceDefinition<UpdateTagParameter, void> {
  return {
    section: BlogSection,
    name: UpdateTag,
    validation: Validation.WithJsonSchema(UpdateTagParameterSchema),
    authorization: Authorization.WithLoggedInStatus(),
    call: async (param) => {
      const currentDatetime = new Date();

      const result = await ctx.database.collection(TagCollection).updateOne(
        { _id: new ObjectId(param.id) },
        {
          $set: {
            updatedAt: currentDatetime,

            name: param.name,
          },
        },
      );

      if (result.matchedCount === 0) {
        throw new ServiceError('Entity not found');
      }
    },
  };
}
