import { ObjectId } from 'mongodb';
import { GlobalContext } from 'src/commons/context/global-context';
import { ServiceDefinition, ServiceError } from 'src/commons/service-registry/types';
import { Authorization } from 'src/commons/service-registry/service-helper-authorization';
import { Validation } from 'src/commons/service-registry/service-helper-validation';
import { BlogSection } from '../constants';
import {
  TagCollection,
  GetTag,
} from './constants';
import {
  TagResult,
  GetTagParameter,
  GetTagParameterSchema,
} from './types';

export function BuildGetTagService(ctx: GlobalContext): ServiceDefinition<GetTagParameter, TagResult> {
  return {
    section: BlogSection,
    name: GetTag,
    validation: Validation.WithJsonSchema(GetTagParameterSchema),
    authorization: Authorization.WithLoggedInStatus(),
    call: async (param) => {
      const result = await ctx.database.collection(TagCollection).findOne({ _id: new ObjectId(param.id) });

      if (!result) {
        throw new ServiceError('Entity not found');
      }

      return {
        id: result._id.toHexString(),

        createdAt: result.createdAt,
        updatedAt: result.updatedAt,

        name: result.name,
      };
    },
  };
}
