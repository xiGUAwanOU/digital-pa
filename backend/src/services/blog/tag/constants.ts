export const TagCollection = 'blog_tags';

export const CreateTag = 'create-tag';
export const DeleteTag = 'delete-tag';
export const GetTag = 'get-tag';
export const ListTags = 'list-tags';
export const UpdateTag = 'update-tag';
