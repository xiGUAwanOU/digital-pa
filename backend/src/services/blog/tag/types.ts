import { MultiLangContent, MultiLangContentSchema } from '../types';

interface TagId {
  id: string;
}

interface TagData {
  name: MultiLangContent<string>;
}

interface TagMetadata {
  createdAt: string;
  updatedAt: string;
}

const idSchema = { type: 'string', pattern: '^[0-9a-f]{24}$' };

const nameSchema = MultiLangContentSchema({ type: 'string' });

const TagIdSchema = {
  properties: {
    id: idSchema,
  },
  required: [ 'id' ],
};

const TagDataSchema = {
  properties: {
    name: nameSchema,
  },
  required: [ 'name' ],
};

export type CreateTagParameter = TagData;

export const CreateTagParameterSchema = {
  type: 'object',
  properties: {
    ...TagDataSchema.properties,
  },
  required: [
    ...TagDataSchema.required,
  ],
};

export type CreateTagResult = TagId;

// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface ListTagsParameter {}

export const ListTagsParameterSchema = {
  type: 'object',
  properties: {},
  required: [],
  additionalProperties: false,
};

export type GetTagParameter = TagId;

export const GetTagParameterSchema = {
  type: 'object',
  properties: {
    ...TagIdSchema.properties,
  },
  required: [
    ...TagIdSchema.required,
  ],
  additionalProperties: false,
};

export interface TagResult extends TagId, TagMetadata, TagData {}

export interface UpdateTagParameter extends TagId, TagData {}

export const UpdateTagParameterSchema = {
  type: 'object',
  properties: {
    ...TagIdSchema.properties,
    ...TagDataSchema.properties,
  },
  required: [
    ...TagIdSchema.required,
    ...TagDataSchema.required,
  ],
  additionalProperties: false,
};

export type DeleteTagParameter = TagId;

export const DeleteTagParameterSchema = {
  type: 'object',
  properties: {
    ...TagIdSchema.properties,
  },
  required: [
    ...TagIdSchema.required,
  ],
  additionalProperties: false,
};
