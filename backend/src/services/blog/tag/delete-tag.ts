import { ObjectId } from 'mongodb';
import { ServiceDefinition, ServiceError } from 'src/commons/service-registry/types';
import { GlobalContext } from 'src/commons/context/global-context';
import { Validation } from 'src/commons/service-registry/service-helper-validation';
import { Authorization } from 'src/commons/service-registry/service-helper-authorization';
import { BlogSection } from '../constants';
import {
  TagCollection,
  DeleteTag,
} from './constants';
import {
  DeleteTagParameter, DeleteTagParameterSchema,
} from './types';

export function BuildDeleteTagService(ctx: GlobalContext): ServiceDefinition<DeleteTagParameter, void> {
  return {
    section: BlogSection,
    name: DeleteTag,
    validation: Validation.WithJsonSchema(DeleteTagParameterSchema),
    authorization: Authorization.WithLoggedInStatus(),
    call: async (param) => {
      const result = await ctx.database.collection(TagCollection).deleteOne({ _id: new ObjectId(param.id) });

      if (result.deletedCount === 0) {
        throw new ServiceError('Entity not found');
      }
    },
  };
}
