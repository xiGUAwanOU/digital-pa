import { RegisterEndpoint } from 'src/commons/framework-integration/register-endpoint';
import { GlobalContext } from 'src/commons/context/global-context';
import { BlogSection } from '../constants';
import {
  CreateTag,
  DeleteTag,
  GetTag,
  ListTags,
  UpdateTag,
} from './constants';
import { BuildCreateTagService } from './create-tag';
import { BuildGetTagService } from './get-tag';
import { BuildListTagsService } from './list-tags';
import { BuildUpdateTagService } from './update-tag';
import { BuildDeleteTagService } from './delete-tag';

export function RegisterTagServices(ctx: GlobalContext) {
  ctx.serviceRegistry.registerService(BuildCreateTagService(ctx));
  ctx.serviceRegistry.registerService(BuildGetTagService(ctx));
  ctx.serviceRegistry.registerService(BuildListTagsService(ctx));
  ctx.serviceRegistry.registerService(BuildUpdateTagService(ctx));
  ctx.serviceRegistry.registerService(BuildDeleteTagService(ctx));
}

export function RegsiterTagEndpoints(ctx: GlobalContext) {
  RegisterEndpoint(ctx, BlogSection, CreateTag);
  RegisterEndpoint(ctx, BlogSection, GetTag);
  RegisterEndpoint(ctx, BlogSection, ListTags);
  RegisterEndpoint(ctx, BlogSection, UpdateTag);
  RegisterEndpoint(ctx, BlogSection, DeleteTag);
}
