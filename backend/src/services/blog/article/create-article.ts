import { ObjectId } from 'mongodb';
import { ServiceDefinition } from 'src/commons/service-registry/types';
import { Authorization } from 'src/commons/service-registry/service-helper-authorization';
import { Validation } from 'src/commons/service-registry/service-helper-validation';
import { GlobalContext } from 'src/commons/context/global-context';
import { BlogSection } from '../constants';
import {
  ArticleCollection,
  CreateArticle,
} from './constants';
import {
  CreateArticleParameter,
  CreateArticleParameterSchema,
  CreateArticleResult,
} from './types';

export function BuildCreateArticleService(
  ctx: GlobalContext,
): ServiceDefinition<CreateArticleParameter, CreateArticleResult> {
  return {
    section: BlogSection,
    name: CreateArticle,
    validation: Validation.WithJsonSchema(CreateArticleParameterSchema),
    authorization: Authorization.WithLoggedInStatus(),
    call: async (param) => {
      const currentDatetime = new Date();

      const result = await ctx.database.collection(ArticleCollection).insertOne({
        createdAt: currentDatetime,
        updatedAt: currentDatetime,

        title: param.title,
        content: param.content,
        tagIds: param.tagIds.map((tagId) => new ObjectId(tagId)),
        time: {
          datetime: new Date(param.time.datetime),
          timezone: param.time.timezone,
        },
        location: param.location,
        thumbnailImageUrl: param.thumbnailImageUrl,
        published: param.published,
      });

      return { id: result.insertedId.toHexString() };
    },
  };
}
