import { RegisterEndpoint } from 'src/commons/framework-integration/register-endpoint';
import { GlobalContext } from 'src/commons/context/global-context';
import { BlogSection } from '../constants';
import {
  CreateArticle,
  DeleteArticle,
  GetArticle,
  ListArticles,
  UpdateArticle,
} from './constants';
import { BuildCreateArticleService } from './create-article';
import { BuildGetArticleService } from './get-article';
import { BuildListArticlesService } from './list-articles';
import { BuildUpdateArticleService } from './update-article';
import { BuildDeleteArticleService } from './delete-article';

export function RegisterArticleServices(ctx: GlobalContext) {
  ctx.serviceRegistry.registerService(BuildCreateArticleService(ctx));
  ctx.serviceRegistry.registerService(BuildGetArticleService(ctx));
  ctx.serviceRegistry.registerService(BuildListArticlesService(ctx));
  ctx.serviceRegistry.registerService(BuildUpdateArticleService(ctx));
  ctx.serviceRegistry.registerService(BuildDeleteArticleService(ctx));
}

export function RegsiterArticleEndpoints(ctx: GlobalContext) {
  RegisterEndpoint(ctx, BlogSection, CreateArticle);
  RegisterEndpoint(ctx, BlogSection, GetArticle);
  RegisterEndpoint(ctx, BlogSection, ListArticles);
  RegisterEndpoint(ctx, BlogSection, UpdateArticle);
  RegisterEndpoint(ctx, BlogSection, DeleteArticle);
}
