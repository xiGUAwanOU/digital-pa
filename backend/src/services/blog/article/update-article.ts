import { ObjectId } from 'mongodb';
import { GlobalContext } from 'src/commons/context/global-context';
import { ServiceDefinition, ServiceError } from 'src/commons/service-registry/types';
import { Authorization } from 'src/commons/service-registry/service-helper-authorization';
import { Validation } from 'src/commons/service-registry/service-helper-validation';
import { BlogSection } from '../constants';
import {
  ArticleCollection,
  UpdateArticle,
} from './constants';
import {
  UpdateArticleParameter,
  UpdateArticleParameterSchema,
} from './types';

export function BuildUpdateArticleService(ctx: GlobalContext): ServiceDefinition<UpdateArticleParameter, void> {
  return {
    section: BlogSection,
    name: UpdateArticle,
    validation: Validation.WithJsonSchema(UpdateArticleParameterSchema),
    authorization: Authorization.WithLoggedInStatus(),
    call: async (param) => {
      const currentDatetime = new Date();

      const result = await ctx.database.collection(ArticleCollection).updateOne(
        { _id: new ObjectId(param.id) },
        {
          $set: {
            updatedAt: currentDatetime,

            title: param.title,
            content: param.content,
            tagIds: param.tagIds.map((tagId) => new ObjectId(tagId)),
            time: {
              datetime: new Date(param.time.datetime),
              timezone: param.time.timezone,
            },
            location: param.location,
            thumbnailImageUrl: param.thumbnailImageUrl,
            published: param.published,
          },
        },
      );

      if (result.matchedCount === 0) {
        throw new ServiceError('Entity not found');
      }
    },
  };
}
