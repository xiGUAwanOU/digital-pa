export const ArticleCollection = 'blog_articles';

export const CreateArticle = 'create-article';
export const DeleteArticle = 'delete-article';
export const GetArticle = 'get-article';
export const ListArticles = 'list-articles';
export const UpdateArticle = 'update-article';
