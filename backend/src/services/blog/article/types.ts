import { MultiLangContent, MultiLangContentSchema } from '../types';

interface ArticleId {
  id: string;
}

interface ArticleData {
  title: MultiLangContent<string>;
  content: ArticleContentData | null;
  tagIds: string[];
  time: ArticleTimeData,
  location: ArticleLocationData;
  thumbnailImageUrl: string | null;
  published: boolean;
}

interface ArticleContentData {
  content: MultiLangContent<string>;
  excerpt: MultiLangContent<string>;
}

interface ArticleTimeData {
  datetime: string;
  timezone: string;
}

interface ArticleLocationData {
  name: MultiLangContent<string>;
  latitude: number | null;
  longitude: number | null;
}

interface ArticleMetadata {
  createdAt: string;
  updatedAt: string;
}

const idSchema = { type: 'string', pattern: '^[0-9a-f]{24}$' };

const titleSchema = MultiLangContentSchema({ type: 'string' });

const contentSchema = {
  type: [ 'object', 'null' ],
  properties: {
    content: MultiLangContentSchema({ type: 'string' }),
    excerpt: MultiLangContentSchema({ type: 'string' }),
  },
  required: [ 'content', 'excerpt' ],
};

const tagIdsSchema = { type: 'array', items: { ...idSchema } };
const timeSchema = {
  type: 'object',
  properties: {
    datetime: { type: 'string', format: 'date-time' },
    timezone: { type: 'string' },
  },
};
const locationSchema = {
  type: 'object',
  properties: {
    name: MultiLangContentSchema({ type: 'string' }),
    latitude: { type: [ 'number', 'null' ], minimum: -90, maximum: 90 },
    longitude: { type: [ 'number', 'null' ], minimum: -180, maximum: 180 },
  },
};
const thumbnailImageUrlSchema = { type: [ 'string', 'null' ], format: 'uri' };

const publishedSchema = { type: 'boolean' };

const ArticleIdSchema = {
  properties: {
    id: idSchema,
  },
  required: [ 'id' ],
};

const ArticleDataSchema = {
  properties: {
    title: titleSchema,
    content: contentSchema,
    tagIds: tagIdsSchema,
    time: timeSchema,
    location: locationSchema,
    thumbnailImageUrl: thumbnailImageUrlSchema,
    published: publishedSchema,
  },
  required: [
    'title',
    'content',
    'tagIds',
    'time',
    'location',
    'thumbnailImageUrl',
    'published',
  ],
};

export type CreateArticleParameter = ArticleData;

export const CreateArticleParameterSchema = {
  type: 'object',
  properties: {
    ...ArticleDataSchema.properties,
  },
  required: [
    ...ArticleDataSchema.required,
  ],
  additionalProperties: false,
};

export type CreateArticleResult = ArticleId;

export interface ListArticlesParameter {
  tagIds: string[];
}

export const ListArticlesParameterSchema = {
  type: 'object',
  properties: {
    tagIds: tagIdsSchema,
  },
  required: [
    'tagIds',
  ],
  additionalProperties: false,
};

export type GetArticleParameter = ArticleId;

export const GetArticleParameterSchema = {
  type: 'object',
  properties: {
    ...ArticleIdSchema.properties,
  },
  required: [
    ...ArticleIdSchema.required,
  ],
  additionalProperties: false,
};

export interface ArticleResult extends ArticleId, ArticleMetadata, ArticleData {}

export interface UpdateArticleParameter extends ArticleId, ArticleData {}

export const UpdateArticleParameterSchema = {
  type: 'object',
  properties: {
    ...ArticleIdSchema.properties,
    ...ArticleDataSchema.properties,
  },
  required: [
    ...ArticleIdSchema.required,
    ...ArticleDataSchema.required,
  ],
  additionalProperties: false,
};

export type DeleteArticleParameter = ArticleId;

export const DeleteArticleParameterSchema = {
  type: 'object',
  properties: {
    ...ArticleIdSchema.properties,
  },
  required: [
    ...ArticleIdSchema.required,
  ],
  additionalProperties: false,
};
