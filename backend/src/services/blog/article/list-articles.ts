import { ObjectId } from 'mongodb';
import { GlobalContext } from 'src/commons/context/global-context';
import { ServiceDefinition } from 'src/commons/service-registry/types';
import { Authorization } from 'src/commons/service-registry/service-helper-authorization';
import { Validation } from 'src/commons/service-registry/service-helper-validation';
import { BlogSection } from '../constants';
import {
  ArticleCollection,
  ListArticles,
} from './constants';
import {
  ArticleResult,
  ListArticlesParameter,
  ListArticlesParameterSchema,
} from './types';

export function BuildListArticlesService(
  ctx: GlobalContext,
): ServiceDefinition<ListArticlesParameter, ArticleResult[]> {
  return {
    section: BlogSection,
    name: ListArticles,
    validation: Validation.WithJsonSchema(ListArticlesParameterSchema),
    authorization: Authorization.WithLoggedInStatus(),
    call: async (param) => {
      const articleFilter = param.tagIds.length !== 0
        ? { tagIds: { $all: param.tagIds.map((tagId) => new ObjectId(tagId)) } }
        : {};

      const results = await ctx.database.collection(ArticleCollection)
        .find(articleFilter)
        .sort({ 'time.datetime': -1 })
        .toArray();

      return results.map((result) => ({
        id: result._id.toHexString(),

        createdAt: result.createdAt,
        updatedAt: result.updatedAt,

        title: result.title,
        content: result.content,
        tagIds: result.tagIds.map((tagId: ObjectId) => tagId.toHexString()),
        time: result.time,
        location: result.location,
        thumbnailImageUrl: result.thumbnailImageUrl,
        published: result.published,
      }));
    },
  };
}
