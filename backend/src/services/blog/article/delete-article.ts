import { ObjectId } from 'mongodb';
import { ServiceDefinition, ServiceError } from 'src/commons/service-registry/types';
import { GlobalContext } from 'src/commons/context/global-context';
import { Validation } from 'src/commons/service-registry/service-helper-validation';
import { Authorization } from 'src/commons/service-registry/service-helper-authorization';
import { BlogSection } from '../constants';
import {
  ArticleCollection,
  DeleteArticle,
} from './constants';
import {
  DeleteArticleParameter,
  DeleteArticleParameterSchema,
} from './types';

export function BuildDeleteArticleService(ctx: GlobalContext): ServiceDefinition<DeleteArticleParameter, void> {
  return {
    section: BlogSection,
    name: DeleteArticle,
    validation: Validation.WithJsonSchema(DeleteArticleParameterSchema),
    authorization: Authorization.WithLoggedInStatus(),
    call: async (param) => {
      const result = await ctx.database.collection(ArticleCollection).deleteOne({ _id: new ObjectId(param.id) });

      if (result.deletedCount === 0) {
        throw new ServiceError('Entity not found');
      }
    },
  };
}
