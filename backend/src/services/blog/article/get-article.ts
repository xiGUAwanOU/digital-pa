import { ObjectId } from 'mongodb';
import { GlobalContext } from 'src/commons/context/global-context';
import { ServiceDefinition, ServiceError } from 'src/commons/service-registry/types';
import { Authorization } from 'src/commons/service-registry/service-helper-authorization';
import { Validation } from 'src/commons/service-registry/service-helper-validation';
import { BlogSection } from '../constants';
import {
  ArticleCollection,
  GetArticle,
} from './constants';
import {
  ArticleResult,
  GetArticleParameter,
  GetArticleParameterSchema,
} from './types';

export function BuildGetArticleService(ctx: GlobalContext): ServiceDefinition<GetArticleParameter, ArticleResult> {
  return {
    section: BlogSection,
    name: GetArticle,
    validation: Validation.WithJsonSchema(GetArticleParameterSchema),
    authorization: Authorization.WithLoggedInStatus(),
    call: async (param) => {
      const result = await ctx.database.collection(ArticleCollection).findOne({ _id: new ObjectId(param.id) });

      if (!result) {
        throw new ServiceError('Entity not found');
      }

      return {
        id: result._id.toHexString(),

        createdAt: result.createdAt,
        updatedAt: result.updatedAt,

        title: result.title,
        content: result.content,
        tagIds: result.tagIds.map((tagId: ObjectId) => tagId.toHexString()),
        time: result.time,
        location: result.location,
        thumbnailImageUrl: result.thumbnailImageUrl,
        published: result.published,
      };
    },
  };
}
