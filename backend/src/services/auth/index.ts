import { Response } from 'express';

import { BuildLoginService } from './login';

import { GlobalContext } from 'src/commons/context/global-context';
import { RegisterEndpoint } from 'src/commons/framework-integration/register-endpoint';

import { LoginResult } from './types';
import { AuthSection, Login } from './constants';

export function RegisterAuthServices(ctx: GlobalContext) {
  ctx.serviceRegistry.registerService(BuildLoginService(ctx));
}

export function RegisterAuthEndpoints(ctx: GlobalContext) {
  RegisterEndpoint(ctx, AuthSection, Login, {
    resultHandler: (result: LoginResult, res: Response) => {
      res.status(200);
      res.cookie('Authorization', result.jwt, { maxAge: ctx.config.jwt.expiresInSeconds * 1000, httpOnly: true });
      res.json(result);
      res.end();
    },
  });
}
