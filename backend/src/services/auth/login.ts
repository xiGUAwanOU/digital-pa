import * as jwt from 'jsonwebtoken';
import { GlobalContext } from 'src/commons/context/global-context';
import { ServiceDefinition, ServiceError } from 'src/commons/service-registry/types';
import { Authorization } from 'src/commons/service-registry/service-helper-authorization';
import { Validation } from 'src/commons/service-registry/service-helper-validation';
import {
  LoginParameter,
  LoginParameterSchema,
  LoginResult,
} from './types';
import {
  AuthSection,
  Login,
} from './constants';

export function BuildLoginService(ctx: GlobalContext): ServiceDefinition<LoginParameter, LoginResult> {
  function signJwt(ctx: GlobalContext): string {
    return jwt.sign({}, ctx.config.jwt.secret, { expiresIn: ctx.config.jwt.expiresInSeconds });
  }

  return {
    section: AuthSection,
    name: Login,
    validation: Validation.WithJsonSchema(LoginParameterSchema),
    authorization: Authorization.NotRequired(),
    call: (param) => {
      if (param.password !== ctx.config.primaryPassword) {
        throw new ServiceError('Incorrect username or password');
      }

      return { jwt: signJwt(ctx) };
    },
  };
}
