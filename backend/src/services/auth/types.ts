export interface LoginParameter {
  password: string;
}

export const LoginParameterSchema = {
  type: 'object',
  properties: {
    password: { type: 'string' },
  },
  required: [ 'password' ],
  additionalProperties: false,
};

export interface LoginResult {
  jwt: string;
}
