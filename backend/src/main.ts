import { BuildApp, StartApp } from './app';

(async () => {
  StartApp(await BuildApp());
})();
