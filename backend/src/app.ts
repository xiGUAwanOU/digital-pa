import * as fs from 'fs';

import * as express from 'express';
import { Express } from 'express';
import { Db, MongoClient } from 'mongodb';

import { GlobalConfig } from './commons/config/global-config';
import { GlobalContext } from './commons/context/global-context';

import { ServiceRegistry } from './commons/service-registry/service-registry';

import {
  RegisterBlogServices,
  RegsiterBlogEndpoints,
} from './services/blog';
import {
  RegisterAuthEndpoints,
  RegisterAuthServices,
} from './services/auth';
import {
  RegisterTaskEndpoints,
  RegisterTaskServices,
} from './services/task';

export async function BuildApp(): Promise<GlobalContext> {
  const config = readConfiguration();
  const express = initExpress();
  const databaseConnection = initMongoDbConnection(config);
  const database = initMongoDbDatabase(config, databaseConnection);
  const serviceRegistry = initServiceRegistry();

  const ctx: GlobalContext = {
    config, express, databaseConnection, database, serviceRegistry,
  };

  RegisterAuthServices(ctx);
  RegisterBlogServices(ctx);
  RegisterTaskServices(ctx);

  RegisterAuthEndpoints(ctx);
  RegsiterBlogEndpoints(ctx);
  RegisterTaskEndpoints(ctx);

  return ctx;
}

export async function StartApp(ctx: GlobalContext) {
  await ctx.databaseConnection.connect();

  ctx.express.listen(3000, () => {
    console.log('Server started');
  });
}

export async function StopApp(ctx: GlobalContext) {
  await ctx.databaseConnection.close();
}

function readConfiguration(): GlobalConfig {
  const configFilePath = `./config.${process.env.NODE_ENV}.json`;
  const configFileContent: string = fs.readFileSync(configFilePath, { encoding: 'utf-8' });
  return JSON.parse(configFileContent);
}

function initExpress(): Express {
  return express();
}

function initMongoDbConnection(config: GlobalConfig): MongoClient {
  // eslint-disable-next-line max-len
  const mongoUrl = `mongodb://${config.database.user}:${config.database.password}@${config.database.host}:${config.database.port}`;
  return new MongoClient(mongoUrl);
}

function initMongoDbDatabase(config: GlobalConfig, databaseConnection: MongoClient): Db {
  return databaseConnection.db(config.database.name);
}

function initServiceRegistry(): ServiceRegistry {
  return new ServiceRegistry();
}
