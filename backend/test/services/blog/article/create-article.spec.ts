import { ObjectId } from 'mongodb';
import * as request from 'supertest';

import { BuildApp } from 'src/app';
import { GlobalContext } from 'src/commons/context/global-context';
import { ArticleCollection } from 'src/services/blog/article/constants';
import { clearDatabase, getValidJwt } from 'test/services/utilities';

describe('blog/create-article', () => {
  let ctx: GlobalContext;
  let validJwt: string;

  beforeAll(async () => {
    ctx = await BuildApp();
    await ctx.databaseConnection.connect();
    validJwt = getValidJwt(ctx);
  });

  afterAll(async () => {
    await ctx.databaseConnection.close();
  });

  beforeEach(async () => {
    await clearDatabase(ctx);
  });

  describe('/api/blog/create-article', () => {
    it('should insert db entity with minimal field requirements', async () => {
      const response = await request(ctx.express)
        .post('/api/blog/create-article')
        .set('Cookie', `Authorization=${validJwt}`)
        .send({
          title: {
            en: 'Test Article',
          },
          content: null,
          tagIds: [],
          time: {
            datetime: '2021-11-07T00:00:00.000Z',
            timezone: 'Europe/Berlin',
          },
          location: null,
          thumbnailImageUrl: null,
          published: false,
        });

      expect(response.status).toEqual(200);
      expect(response.body).toEqual({ id: expect.any(String) });

      expect(await ctx.database.collection(ArticleCollection).find().toArray()).toEqual([
        {
          _id: expect.anything(),

          createdAt: expect.any(Date),
          updatedAt: expect.any(Date),

          title: {
            en: 'Test Article',
          },
          content: null,
          tagIds: [],
          time: {
            datetime: new Date('2021-11-07T00:00:00.000Z'),
            timezone: 'Europe/Berlin',
          },
          location: null,
          thumbnailImageUrl: null,
          published: false,
        },
      ]);
    });

    it('should insert db entity and return inserted ID', async () => {
      const response = await request(ctx.express)
        .post('/api/blog/create-article')
        .set('Cookie', `Authorization=${validJwt}`)
        .send({
          title: {
            en: 'Test Article',
          },
          content: {
            content: {
              en: '<p>Hello world!</p>',
            },
            excerpt: {
              en: 'Hello world!',
            },
          },
          tagIds: [ '000000000000000000000000' ],
          time: {
            datetime: '2021-11-07T00:00:00.000Z',
            timezone: 'Europe/Berlin',
          },
          location: {
            name: {
              en: 'Home',
            },
            latitude: 0,
            longitude: 0,
          },
          thumbnailImageUrl: 'http://example.org/image.jpg',
          published: false,
        });

      expect(response.status).toEqual(200);
      expect(response.body).toEqual({ id: expect.any(String) });

      expect(await ctx.database.collection(ArticleCollection).find().toArray()).toEqual([
        {
          _id: expect.anything(),

          createdAt: expect.any(Date),
          updatedAt: expect.any(Date),

          title: {
            en: 'Test Article',
          },
          content: {
            content: {
              en: '<p>Hello world!</p>',
            },
            excerpt: {
              en: 'Hello world!',
            },
          },
          tagIds: [ new ObjectId('000000000000000000000000') ],
          time: {
            datetime: new Date('2021-11-07T00:00:00.000Z'),
            timezone: 'Europe/Berlin',
          },
          location: {
            name: {
              en: 'Home',
            },
            latitude: 0,
            longitude: 0,
          },
          thumbnailImageUrl: 'http://example.org/image.jpg',
          published: false,
        },
      ]);
    });
  });
});