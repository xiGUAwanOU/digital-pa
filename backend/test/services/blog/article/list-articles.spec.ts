import * as request from 'supertest';

import { BuildApp } from 'src/app';
import { GlobalContext } from 'src/commons/context/global-context';
import { BlogSection } from 'src/services/blog/constants';
import { CreateArticleParameter, CreateArticleResult } from 'src/services/blog/article/types';
import { clearDatabase, getValidJwt } from 'test/services/utilities';

describe('blog/list-articles', () => {
  let ctx: GlobalContext;
  let validJwt: string;

  beforeAll(async () => {
    ctx = await BuildApp();
    await ctx.databaseConnection.connect();
    validJwt = getValidJwt(ctx);
  });

  afterAll(async () => {
    await ctx.databaseConnection.close();
  });

  beforeEach(async () => {
    await clearDatabase(ctx);
  });

  describe('/api/blog/list-articles', () => {
    beforeEach(async () => {
      await ctx.serviceRegistry.callService<CreateArticleParameter, CreateArticleResult>(
        BlogSection,
        'create-article',
        {
          title: {
            en: 'Test Article',
          },
          content: {
            content: {
              en: '<p>Hello world!</p>',
            },
            excerpt: {
              en: 'Hello world!',
            },
          },
          tagIds: [ '000000000000000000000000' ],
          time: {
            datetime: '2021-11-07T00:00:00.000Z',
            timezone: 'Europe/Berlin',
          },
          location: {
            name: {
              en: 'Home',
            },
            latitude: 0,
            longitude: 0,
          },
          thumbnailImageUrl: 'http://example.org/image.jpg',
          published: false,
        },
        { userLoggedIn: true },
      );

      await ctx.serviceRegistry.callService<CreateArticleParameter, CreateArticleResult>(
        BlogSection,
        'create-article',
        {
          title: {
            en: 'Another Test Article',
          },
          content: {
            content: {
              en: '<p>Another hello world!</p>',
            },
            excerpt: {
              en: 'Another hello world!',
            },
          },
          tagIds: [ '000000000000000000000001' ],
          time: {
            datetime: '2021-11-08T00:00:00.000Z',
            timezone: 'Europe/Berlin',
          },
          location: {
            name: {
              en: 'Somewhere Else',
            },
            latitude: 0,
            longitude: 0,
          },
          thumbnailImageUrl: 'http://example.org/another-image.jpg',
          published: false,
        },
        { userLoggedIn: true },
      );
    });

    it('should return all inserted articles', async () => {
      const response = await request(ctx.express)
        .post('/api/blog/list-articles')
        .set('Cookie', `Authorization=${validJwt}`)
        .send({ tagIds: [] });

      expect(response.status).toEqual(200);
      expect(response.body).toEqual([
        {
          id: expect.any(String),

          createdAt: expect.any(String),
          updatedAt: expect.any(String),

          title: {
            en: 'Another Test Article',
          },
          content: {
            content: {
              en: '<p>Another hello world!</p>',
            },
            excerpt: {
              en: 'Another hello world!',
            },
          },
          tagIds: [ '000000000000000000000001' ],
          time: {
            datetime: '2021-11-08T00:00:00.000Z',
            timezone: 'Europe/Berlin',
          },
          location: {
            name: {
              en: 'Somewhere Else',
            },
            latitude: 0,
            longitude: 0,
          },
          thumbnailImageUrl: 'http://example.org/another-image.jpg',
          published: false,
        },
        {
          id: expect.any(String),

          createdAt: expect.any(String),
          updatedAt: expect.any(String),

          title: {
            en: 'Test Article',
          },
          content: {
            content: {
              en: '<p>Hello world!</p>',
            },
            excerpt: {
              en: 'Hello world!',
            },
          },
          tagIds: [ '000000000000000000000000' ],
          time: {
            datetime: '2021-11-07T00:00:00.000Z',
            timezone: 'Europe/Berlin',
          },
          location: {
            name: {
              en: 'Home',
            },
            latitude: 0,
            longitude: 0,
          },
          thumbnailImageUrl: 'http://example.org/image.jpg',
          published: false,
        },
      ]);
    });

    it('should filter articles with tags', async () => {
      const response = await request(ctx.express)
        .post('/api/blog/list-articles')
        .set('Cookie', `Authorization=${validJwt}`)
        .send({ tagIds: [ '000000000000000000000000' ] });

      expect(response.status).toEqual(200);
      expect(response.body).toEqual([
        {
          id: expect.any(String),

          createdAt: expect.any(String),
          updatedAt: expect.any(String),

          title: {
            en: 'Test Article',
          },
          content: {
            content: {
              en: '<p>Hello world!</p>',
            },
            excerpt: {
              en: 'Hello world!',
            },
          },
          tagIds: [ '000000000000000000000000' ],
          time: {
            datetime: '2021-11-07T00:00:00.000Z',
            timezone: 'Europe/Berlin',
          },
          location: {
            name: {
              en: 'Home',
            },
            latitude: 0,
            longitude: 0,
          },
          thumbnailImageUrl: 'http://example.org/image.jpg',
          published: false,
        },
      ]);
    });
  });
});
