import * as request from 'supertest';

import { BuildApp } from 'src/app';
import { GlobalContext } from 'src/commons/context/global-context';
import { BlogSection } from 'src/services/blog/constants';
import { ArticleCollection } from 'src/services/blog/article/constants';
import { CreateArticleParameter, CreateArticleResult } from 'src/services/blog/article/types';
import { clearDatabase, getValidJwt } from 'test/services/utilities';

describe('blog/delete-article', () => {
  let ctx: GlobalContext;
  let validJwt: string;

  beforeAll(async () => {
    ctx = await BuildApp();
    await ctx.databaseConnection.connect();
    validJwt = getValidJwt(ctx);
  });

  afterAll(async () => {
    await ctx.databaseConnection.close();
  });

  beforeEach(async () => {
    await clearDatabase(ctx);
  });

  describe('/api/blog/delete-article', () => {
    it('should delete db entity', async () => {
      const result = await ctx.serviceRegistry.callService<CreateArticleParameter, CreateArticleResult>(
        BlogSection,
        'create-article',
        {
          title: {
            en: 'Test Article',
          },
          content: {
            content: {
              en: '<p>Hello world!</p>',
            },
            excerpt: {
              en: 'Hello world!',
            },
          },
          tagIds: [ '000000000000000000000000' ],
          time: {
            datetime: '2021-11-07T00:00:00.000Z',
            timezone: 'Europe/Berlin',
          },
          location: {
            name: {
              en: 'Home',
            },
            latitude: 0,
            longitude: 0,
          },
          thumbnailImageUrl: 'http://example.org/image.jpg',
          published: false,
        },
        { userLoggedIn: true },
      );

      const response = await request(ctx.express)
        .post('/api/blog/delete-article')
        .set('Cookie', `Authorization=${validJwt}`)
        .send({ id: result.id });

      expect(response.status).toEqual(200);
      expect(await ctx.database.collection(ArticleCollection).find().toArray()).toEqual([]);
    });
  });
});