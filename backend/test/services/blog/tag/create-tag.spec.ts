import * as request from 'supertest';

import { BuildApp } from 'src/app';
import { GlobalContext } from 'src/commons/context/global-context';
import { TagCollection } from 'src/services/blog/tag/constants';
import { clearDatabase, getValidJwt } from 'test/services/utilities';

describe('blog/create-tag', () => {
  let ctx: GlobalContext;
  let validJwt: string;

  beforeAll(async () => {
    ctx = await BuildApp();
    await ctx.databaseConnection.connect();
    validJwt = getValidJwt(ctx);
  });

  afterAll(async () => {
    await ctx.databaseConnection.close();
  });

  beforeEach(async () => {
    await clearDatabase(ctx);
  });

  describe('/api/blog/create-tag', () => {
    it('should insert db entity and return inserted ID', async () => {
      const response = await request(ctx.express)
        .post('/api/blog/create-tag')
        .set('Cookie', `Authorization=${validJwt}`)
        .send({
          name: {
            en: 'Travel',
            de: 'Reise',
            zh: '旅行',
          },
        });

      expect(response.status).toEqual(200);
      expect(response.body).toEqual({ id: expect.any(String) });

      expect(await ctx.database.collection(TagCollection).find().toArray()).toEqual([
        {
          _id: expect.anything(),

          createdAt: expect.any(Date),
          updatedAt: expect.any(Date),

          name: {
            en: 'Travel',
            de: 'Reise',
            zh: '旅行',
          },
        },
      ]);
    });
  });
});