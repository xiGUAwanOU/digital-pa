import * as request from 'supertest';

import { BuildApp } from 'src/app';
import { GlobalContext } from 'src/commons/context/global-context';
import { BlogSection } from 'src/services/blog/constants';
import { TagCollection } from 'src/services/blog/tag/constants';
import { CreateTagParameter, CreateTagResult } from 'src/services/blog/tag/types';
import { clearDatabase, getValidJwt } from 'test/services/utilities';

describe('blog/update-tag', () => {
  let ctx: GlobalContext;
  let validJwt: string;

  beforeAll(async () => {
    ctx = await BuildApp();
    await ctx.databaseConnection.connect();
    validJwt = getValidJwt(ctx);
  });

  afterAll(async () => {
    await ctx.databaseConnection.close();
  });

  beforeEach(async () => {
    await clearDatabase(ctx);
  });

  describe('/api/blog/update-tag', () => {
    it('should update existing tag', async () => {
      const result = await ctx.serviceRegistry.callService<CreateTagParameter, CreateTagResult>(
        BlogSection,
        'create-tag',
        {
          name: {
            en: 'Travel',
            de: 'Reise',
            zh: '旅行',
          },
        },
        { userLoggedIn: true },
      );

      const response = await request(ctx.express)
        .post('/api/blog/update-tag')
        .set('Cookie', `Authorization=${validJwt}`)
        .send({
          id: result.id,

          name: {
            en: 'Motorcycle',
            de: 'Motorrad',
            zh: '摩托车',
          },
        });

      expect(response.status).toEqual(200);

      expect(await ctx.database.collection(TagCollection).find().toArray()).toEqual([
        {
          _id: expect.anything(),

          createdAt: expect.any(Date),
          updatedAt: expect.any(Date),

          name: {
            en: 'Motorcycle',
            de: 'Motorrad',
            zh: '摩托车',
          },
        },
      ]);
    });
  });
});
