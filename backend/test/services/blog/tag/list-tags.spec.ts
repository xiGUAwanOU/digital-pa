import * as request from 'supertest';

import { BuildApp } from 'src/app';
import { GlobalContext } from 'src/commons/context/global-context';
import { BlogSection } from 'src/services/blog/constants';
import { CreateTagParameter, CreateTagResult } from 'src/services/blog/tag/types';
import { clearDatabase, getValidJwt } from 'test/services/utilities';

describe('blog/list-tags', () => {
  let ctx: GlobalContext;
  let validJwt: string;

  beforeAll(async () => {
    ctx = await BuildApp();
    await ctx.databaseConnection.connect();
    validJwt = getValidJwt(ctx);
  });

  afterAll(async () => {
    await ctx.databaseConnection.close();
  });

  beforeEach(async () => {
    await clearDatabase(ctx);
  });

  describe('/api/blog/list-tags', () => {
    it('should return all inserted tags', async () => {
      await ctx.serviceRegistry.callService<CreateTagParameter, CreateTagResult>(
        BlogSection,
        'create-tag',
        {
          name: {
            en: 'Travel',
            de: 'Reise',
            zh: '旅行',
          },
        },
        { userLoggedIn: true },
      );

      await ctx.serviceRegistry.callService<CreateTagParameter, CreateTagResult>(
        BlogSection,
        'create-tag',
        {
          name: {
            en: 'Motorcycle',
            de: 'Motorrad',
            zh: '摩托车',
          },
        },
        { userLoggedIn: true },
      );

      const response = await request(ctx.express)
        .post('/api/blog/list-tags')
        .set('Cookie', `Authorization=${validJwt}`)
        .send({ });

      expect(response.status).toEqual(200);
      expect(response.body).toEqual([
        {
          id: expect.any(String),

          createdAt: expect.any(String),
          updatedAt: expect.any(String),

          name: {
            en: 'Travel',
            de: 'Reise',
            zh: '旅行',
          },
        },
        {
          id: expect.any(String),

          createdAt: expect.any(String),
          updatedAt: expect.any(String),

          name: {
            en: 'Motorcycle',
            de: 'Motorrad',
            zh: '摩托车',
          },
        },
      ]);
    });
  });
});
