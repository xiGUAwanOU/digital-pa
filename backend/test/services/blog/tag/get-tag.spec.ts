import * as request from 'supertest';

import { BuildApp } from 'src/app';
import { GlobalContext } from 'src/commons/context/global-context';
import { BlogSection } from 'src/services/blog/constants';
import { CreateTagParameter, CreateTagResult } from 'src/services/blog/tag/types';
import { clearDatabase, getValidJwt } from 'test/services/utilities';

describe('blog/get-tag', () => {
  let ctx: GlobalContext;
  let validJwt: string;

  beforeAll(async () => {
    ctx = await BuildApp();
    await ctx.databaseConnection.connect();
    validJwt = getValidJwt(ctx);
  });

  afterAll(async () => {
    await ctx.databaseConnection.close();
  });

  beforeEach(async () => {
    await clearDatabase(ctx);
  });

  describe('/api/blog/get-tag', () => {
    it('should return inserted tag', async () => {
      const result = await ctx.serviceRegistry.callService<CreateTagParameter, CreateTagResult>(
        BlogSection,
        'create-tag',
        {
          name: {
            en: 'Travel',
            de: 'Reise',
            zh: '旅行',
          },
        },
        { userLoggedIn: true },
      );

      const response = await request(ctx.express)
        .post('/api/blog/get-tag')
        .set('Cookie', `Authorization=${validJwt}`)
        .send({ id: result.id });

      expect(response.status).toEqual(200);
      expect(response.body).toEqual({
        id: result.id,

        createdAt: expect.any(String),
        updatedAt: expect.any(String),

        name: {
          en: 'Travel',
          de: 'Reise',
          zh: '旅行',
        },
      });
    });
  });
});
