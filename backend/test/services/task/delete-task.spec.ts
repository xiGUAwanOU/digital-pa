import * as request from 'supertest';

import { BuildApp } from 'src/app';
import {
  CreateTask,
  TaskCollection,
  TaskSection,
} from 'src/services/task/constants';
import { GlobalContext } from 'src/commons/context/global-context';
import { CreateTaskParameter, CreateTaskResult } from 'src/services/task/types';
import { clearDatabase, getValidJwt } from 'test/services/utilities';

describe('task/delete-task', () => {
  let ctx: GlobalContext;
  let validJwt: string;

  beforeAll(async () => {
    ctx = await BuildApp();
    await ctx.databaseConnection.connect();
    validJwt = getValidJwt(ctx);
  });

  afterAll(async () => {
    await ctx.databaseConnection.close();
  });

  beforeEach(async () => {
    await clearDatabase(ctx);
  });

  describe('/api/task/delete-task', () => {
    it('should delete db entity with its children', async () => {
      const result = await ctx.serviceRegistry.callService<CreateTaskParameter, CreateTaskResult>(
        TaskSection,
        CreateTask,
        {
          parentId: null,
          content: 'task for test',
          finished: true,
        },
        { userLoggedIn: true },
      );
      await ctx.serviceRegistry.callService<CreateTaskParameter, CreateTaskResult>(
        TaskSection,
        CreateTask,
        {
          parentId: result.id,
          content: 'another task for test',
          finished: true,
        },
        { userLoggedIn: true },
      );

      const response = await request(ctx.express)
        .post('/api/task/delete-task')
        .set('Cookie', `Authorization=${validJwt}`)
        .send({ id: result.id });

      expect(response.status).toEqual(200);

      expect(await ctx.database.collection(TaskCollection).find().toArray()).toEqual([]);
    });
  });
});