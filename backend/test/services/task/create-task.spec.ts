import * as request from 'supertest';

import { BuildApp } from 'src/app';
import { TaskCollection } from 'src/services/task/constants';
import { GlobalContext } from 'src/commons/context/global-context';
import { clearDatabase, getValidJwt } from 'test/services/utilities';

describe('task/create-task', () => {
  let ctx: GlobalContext;
  let validJwt: string;

  beforeAll(async () => {
    ctx = await BuildApp();
    await ctx.databaseConnection.connect();
    validJwt = getValidJwt(ctx);
  });

  afterAll(async () => {
    await ctx.databaseConnection.close();
  });

  beforeEach(async () => {
    await clearDatabase(ctx);
  });

  describe('/api/task/create-task', () => {
    it('should insert db entity and return inserted ID', async () => {
      const response = await request(ctx.express)
        .post('/api/task/create-task')
        .set('Cookie', `Authorization=${validJwt}`)
        .send({
          parentId: null,
          content: 'hello!',
          finished: false,
        });

      expect(response.status).toEqual(200);
      expect(response.body).toEqual({ id: expect.any(String) });

      expect(await ctx.database.collection(TaskCollection).find().toArray()).toEqual([
        {
          _id: expect.anything(),
          parentId: null,
          content: 'hello!',
          finished: false,
          createdAt: expect.any(Date),
          updatedAt: expect.any(Date),
        },
      ]);
    });
  });
});