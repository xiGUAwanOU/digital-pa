import * as request from 'supertest';
import { BuildApp } from 'src/app';
import { GlobalContext } from 'src/commons/context/global-context';

describe('auth/login', () => {
  let ctx: GlobalContext;

  beforeAll(async () => {
    ctx = await BuildApp();
    await ctx.databaseConnection.connect();
  });

  beforeEach(() => {
    jest.spyOn(Date, 'now').mockReturnValue(Date.parse('2022-01-01T00:00:00Z'));
  });

  afterAll(async () => {
    await ctx.databaseConnection.close();
  });

  describe('/api/auth/login', () => {
    it('should return 400 if request is invalid', async () => {
      const response = await request(ctx.express)
        .post('/api/auth/login')
        .send({});

      expect(response.status).toEqual(400);
      expect(response.body).toEqual({
        error: 'ServiceParameterInvalidError',
        message: 'The parameter passed to function auth/login is invalid: {}',
      });
    });

    it('should return 400 if password is incorrect', async () => {
      const response = await request(ctx.express)
        .post('/api/auth/login')
        .send({ password: 'incorrect_password' });

      expect(response.status).toEqual(400);
      expect(response.body).toEqual({
        error: 'ServiceExecutionError',
        message: 'An error occurs while calling Function auth/login: Incorrect username or password',
      });
    });

    it('should return 200 and JWT', async () => {
      const response = await request(ctx.express)
        .post('/api/auth/login')
        .send({ password: 'no_password' });

      expect(response.status).toEqual(200);
      expect(response.get('Set-Cookie')[0]).toEqual(
        'Authorization='
          + 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.'
          + 'eyJpYXQiOjE2NDA5OTUyMDAsImV4cCI6MTY0MTA2MDAwMH0.'
          + 'YE2zvHLElBRU13vzolxCE4za0twb-4nRe8gWu7G3bns; '
          + 'Max-Age=64800; '
          + 'Path=/; '
          + 'Expires=Sat, 01 Jan 2022 18:00:00 GMT; '
          + 'HttpOnly'
      );
    });
  });
});
