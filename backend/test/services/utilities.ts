import * as jwt from 'jsonwebtoken';

import { GlobalContext } from 'src/commons/context/global-context';

export function getValidJwt(ctx: GlobalContext): string {
  return jwt.sign({}, ctx.config.jwt.secret, { expiresIn: ctx.config.jwt.expiresInSeconds });
}

export async function clearDatabase(ctx: GlobalContext) {
  await Promise.all((await ctx.database.collections())
    .map((collection) => ctx.database.dropCollection(collection.collectionName)));
}
