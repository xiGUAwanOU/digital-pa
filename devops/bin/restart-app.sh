#!/usr/bin/env bash
docker-compose -p digital-pa down
docker load -i digital-pa.tar.gz
docker system prune -f
docker-compose -p digital-pa up -d
