#!/usr/bin/env bash

set -e

if [ $# != 3 ]
then
  echo "Invalid number of parameters!"
  echo "Usage:"
  echo "  devops/build-and-deploy.sh id_file user_at_hostname remote_path"
  exit 1
fi

docker build -f devops/Dockerfile.backend -t digital-pa-backend .
docker build -f devops/Dockerfile.frontend -t digital-pa-frontend .

docker save -o devops/digital-pa.tar digital-pa-backend digital-pa-frontend
gzip devops/digital-pa.tar

scp -i $1 -r \
  devops/.env \
  devops/bin \
  devops/digital-pa.tar.gz \
  devops/docker-compose.yml \
  $2:$3

rm devops/digital-pa.tar.gz

ssh -i $1 -t $2 "cd $3 && $3/bin/restart-app.sh"
